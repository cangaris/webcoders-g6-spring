package pl.cansoft.webcodersspring.components;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.cansoft.webcodersspring.exceptions.AccessDeniedException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.User;
import pl.cansoft.webcodersspring.repositories.UserRepository;

@ExtendWith(MockitoExtension.class)
class CheckAccessComponentTest {

    @InjectMocks
    CheckAccessComponent checkAccessComponent;

    @Mock
    UserRepository userRepository;

    @Mock
    SecurityContext securityContext;

    @Test
    void checkLoggedUserHasAccessToResourcesForOwnId() {
        // given
        Integer id = 5;
        String username = "info@info.pl";
        User user = User.builder()
            .id(5)
            .email(username)
            .build();
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
        Authentication authentication =
            new UsernamePasswordAuthenticationToken(username, "password", List.of(authority));
        SecurityContextHolder.setContext(securityContext);
        // when
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(userRepository.findByEmail(username)).thenReturn(Optional.of(user));
        checkAccessComponent.checkLoggedUserHasAccessToResources(id);
        // then
        Mockito.verify(userRepository).findByEmail(username);
        Mockito.verify(securityContext).getAuthentication();
    }

    @Test
    void checkLoggedUserHasAccessToResourcesForOtherId() {
        // given
        Integer id = 5;
        String username = "info@info.pl";
        User user = User.builder()
            .id(7)
            .email(username)
            .build();
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
        Authentication authentication =
            new UsernamePasswordAuthenticationToken(username, "password", List.of(authority));
        SecurityContextHolder.setContext(securityContext);
        // when
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(userRepository.findByEmail(username)).thenReturn(Optional.of(user));
        // then
        Assertions.assertThrows(AccessDeniedException.class,
            () -> checkAccessComponent.checkLoggedUserHasAccessToResources(id));
        Mockito.verify(userRepository).findByEmail(username);
        Mockito.verify(securityContext).getAuthentication();
    }

    @Test
    void checkLoggedUserHasAccessToResourcesForNonExistingUserId() {
        // given
        Integer id = 5;
        String username = "info@info.pl";
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
        Authentication authentication =
            new UsernamePasswordAuthenticationToken(username, "password", List.of(authority));
        SecurityContextHolder.setContext(securityContext);
        // when
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(userRepository.findByEmail(username)).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class,
            () -> checkAccessComponent.checkLoggedUserHasAccessToResources(id));
        Mockito.verify(userRepository).findByEmail(username);
        Mockito.verify(securityContext).getAuthentication();
    }

    @Test
    void checkLoggedUserHasAccessToResourcesForAdminForDiffUser() {
        // given
        Integer id = 5;
        String username = "info@info.pl";
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_ADMIN");
        Authentication authentication =
            new UsernamePasswordAuthenticationToken(username, "password", List.of(authority));
        SecurityContextHolder.setContext(securityContext);
        // when
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        checkAccessComponent.checkLoggedUserHasAccessToResources(id);
        // then
        Mockito.verify(userRepository, Mockito.never()).findByEmail(username);
        Mockito.verify(securityContext).getAuthentication();
    }
}
