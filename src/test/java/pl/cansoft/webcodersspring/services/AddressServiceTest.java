package pl.cansoft.webcodersspring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.address.AddressSaveDto;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Address;
import pl.cansoft.webcodersspring.repositories.AddressRepository;

@ExtendWith(MockitoExtension.class)
class AddressServiceTest {

    @InjectMocks
    AddressService addressService; // co będę testował

    // wszystkie zależności tego co testuje ---
    @Mock
    AddressRepository addressRepository;
    // ---

    /*
    JUnit - silnik do tetsowania jednostkowego
    Mockito - biblioteka pomocnicza do mockowania zależności
     */
    @Test
    void getAddresses() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<Address> addressList = new ArrayList<>();
        addressList.add(Address.builder().id(10).build());
        addressList.add(Address.builder().id(11).build());
        addressList.add(Address.builder().id(12).build());
        Page<Address> addressPage = new PageImpl<>(addressList);
        // when
        Mockito.when(addressRepository.findAll(pageable)).thenReturn(addressPage);
        Page<AddressDto> addresses = addressService.getAddresses(pageable);
        // then
        Assertions.assertEquals(3, addresses.getContent().size());
        Assertions.assertEquals(10, addresses.getContent().get(0).getId());
        Assertions.assertEquals(11, addresses.getContent().get(1).getId());
        Assertions.assertEquals(12, addresses.getContent().get(2).getId());
        Mockito.verify(addressRepository).findAll(pageable);
    }

    @Test
    void getAddressWhenAddressExists() {
        // given - dane początkowe testu
        Integer id = 3;
        String city = "Warszawa";
        String street = "Marszałkowska";
        Address address = Address.builder()
            .id(id)
            .city(city)
            .street(street)
            .build();
        Optional<Address> optionalAddress = Optional.of(address);
        // when - mockowanie akcji, wywołanie testu
        Mockito.when(addressRepository.findById(id)).thenReturn(optionalAddress);
        AddressDto addressDto = addressService.getAddress(id);
        // then - sprawdzenie czy test się udał, czy założenia spełnione
        // zweryfikuj czy mock został odpalony 1 raz z funckją findById z id 3 (tak - brak error, nie - rzuć error)
        Mockito.verify(addressRepository).findById(id);
        Assertions.assertEquals(id, addressDto.getId()); // sprawdza czy dane z service są zgodne z założonymi w teście
        Assertions.assertEquals(city, addressDto.getCity());
        Assertions.assertEquals(street, addressDto.getStreet());
    }

    @Test
    void getAddressWhenAddressDoesNotExist() {
        // given - dane początkowe testu
        Integer id = 3;
        Optional<Address> optionalAddress = Optional.empty();
        // when - mockowanie akcji, wywołanie testu
        Mockito.when(addressRepository.findById(id)).thenReturn(optionalAddress);
        // then - sprawdzenie czy test się udał, czy założenia spełnione
        Assertions.assertThrows(NotFoundException.class, () -> {
            addressService.getAddress(id);
        });
        Mockito.verify(addressRepository).findById(id);
    }

    @Test
    void insertAddress() {
        // given
        String city = "Warszawa";
        String street = "Marszałkowska";
        Address address = Address.builder()
            .city(city)
            .street(street)
            .build();
        AddressSaveDto addressSaveDto = AddressSaveDto.builder()
            .city(city)
            .street(street)
            .build();
        // when
        Mockito.when(addressRepository.save(address)).thenReturn(address);
        addressService.insertAddress(addressSaveDto);
        // then
        Mockito.verify(addressRepository).save(address); // Mockito.any(Address.class)
    }

    @Test
    void updateAddressWhenAddressExists() {
        // given
        Integer id = 5;
        String city = "Warszawa";
        String street = "Marszałkowska";
        Address address = Address.builder()
            .id(id)
            .city(city)
            .street(street)
            .build();
        AddressSaveDto addressSaveDto = AddressSaveDto.builder()
            .city(city)
            .street(street)
            .build();
        // when
        Mockito.when(addressRepository.existsById(id)).thenReturn(true);
        Mockito.when(addressRepository.save(address)).thenReturn(address);
        addressService.updateAddress(id, addressSaveDto);
        // then
        Mockito.verify(addressRepository).existsById(id); // czy doszło do urochomienia mocka z fn existsById z id 5
        Mockito.verify(addressRepository).save(address); // Mockito.any(Address.class)
    }

    @Test
    void updateAddressWhenAddressDoesNotExist() {
        // given
        Integer id = 5;
        String city = "Warszawa";
        String street = "Marszałkowska";
        AddressSaveDto addressSaveDto = AddressSaveDto.builder()
            .city(city)
            .street(street)
            .build();
        // when
        Mockito.when(addressRepository.existsById(id)).thenReturn(false);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> {
            addressService.updateAddress(id, addressSaveDto);
        });
        Mockito.verify(addressRepository).existsById(id); // czy doszło do urochomienia mocka z fn existsById z id 5
        // sprawdz czy nie doszlo do uruchomienia metody save z jakimkolwiek parametrem
        Mockito.verify(addressRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void deleteAddressWhenAddressExists() {
        // given
        Integer id = 7;
        // when
        Mockito.when(addressRepository.existsById(id)).thenReturn(true);
        addressService.deleteAddress(id);
        // then
        Mockito.verify(addressRepository).deleteById(id);
    }

    @Test
    void deleteAddressWhenAddressDoesNotExist() {
        // given
        Integer id = 7;
        // when
        Mockito.when(addressRepository.existsById(id)).thenReturn(false);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> {
            addressService.deleteAddress(id);
        });
        Mockito.verify(addressRepository, Mockito.never()).deleteById(id);
    }
}
