package pl.cansoft.webcodersspring.services;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.cansoft.webcodersspring.components.CheckAccessComponent;
import pl.cansoft.webcodersspring.dtos.user.UserDto;
import pl.cansoft.webcodersspring.dtos.user.UserSaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.GovData;
import pl.cansoft.webcodersspring.models.User;
import pl.cansoft.webcodersspring.repositories.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;
    @Mock
    CheckAccessComponent checkAccessComponent;
    @Mock
    PasswordEncoder passwordEncoder;

    @Test
    void getUsers() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<User> list = List.of(User.builder()
            .id(7)
            .addresses(List.of())
            .govData(GovData.builder().build())
            .build());
        Page<User> page = new PageImpl<>(list);
        // when
        Mockito.when(userRepository.findAll(pageable)).thenReturn(page);
        Page<UserDto> response = userService.getUsers(pageable);
        // then
        Mockito.verify(userRepository).findAll(pageable);
        Assertions.assertEquals(1, response.getContent().size());
        Assertions.assertEquals(7, response.getContent().getFirst().getId());
    }

    @Test
    void getUserExisting() {
        // given
        Integer id = 6;
        User user = User.builder()
            .id(id)
            .addresses(List.of())
            .govData(GovData.builder().build())
            .build();
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        UserDto response = userService.getUser(id);
        // then
        Assertions.assertEquals(6, response.getId());
        Mockito.verify(userRepository).findById(id);
    }

    @Test
    void getUserNonExisting() {
        // given
        Integer id = 6;
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> userService.getUser(id));
        Mockito.verify(userRepository).findById(id);
    }

    @Test
    void insertUserWithoutConflict() {
        // given
        User user = User.builder()
            .govData(GovData.builder()
                .nip("222")
                .pesel("111")
                .build())
            .email("info@info.pl")
            .password("pass")
            .build();
        UserSaveDto userSaveDto = UserSaveDto.builder()
            .nip("222")
            .pesel("111")
            .email("info@info.pl")
            .password("pass")
            .addresses(List.of())
            .build();
        // when
        Mockito.when(userRepository.existsByEmailOrGovDataPeselOrGovDataNip(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip())).thenReturn(false);
        userService.insertUser(userSaveDto);
        // then
        Mockito.verify(userRepository).existsByEmailOrGovDataPeselOrGovDataNip(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip());
        Mockito.verify(passwordEncoder).encode(userSaveDto.getPassword());
        Mockito.verify(userRepository).save(user);
    }

    @Test
    void insertUserWithConflict() {
        // given
        UserSaveDto userSaveDto = UserSaveDto.builder()
            .nip("222")
            .pesel("111")
            .email("info@info.pl")
            .password("pass")
            .addresses(List.of())
            .build();
        // when
        Mockito.when(userRepository.existsByEmailOrGovDataPeselOrGovDataNip(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip())).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> userService.insertUser(userSaveDto));
        Mockito.verify(userRepository).existsByEmailOrGovDataPeselOrGovDataNip(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip());
        Mockito.verify(passwordEncoder, Mockito.never()).encode(userSaveDto.getPassword());
        Mockito.verify(userRepository, Mockito.never()).save(Mockito.any(User.class));
    }

    @Test
    void updateUserWithoutConflictExistingUser() {
        // given
        Integer id = 13;
        User user = User.builder()
            .id(id)
            .govData(GovData.builder()
                .nip("222")
                .pesel("111")
                .build())
            .email("info@info.pl")
            .password("pass")
            .build();
        UserSaveDto userSaveDto = UserSaveDto.builder()
            .nip("222")
            .pesel("111")
            .email("info@info.pl")
            .password("pass")
            .addresses(List.of())
            .build();
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip(), id)).thenReturn(false);
        userService.updateUser(id, userSaveDto);
        // then
        Mockito.verify(userRepository).existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip(), id);
        Mockito.verify(passwordEncoder).encode(userSaveDto.getPassword());
        Mockito.verify(userRepository).save(user);
        Mockito.verify(userRepository).findById(id);
    }

    @Test
    void updateUserNonExisting() {
        // given
        Integer id = 13;
        UserSaveDto userSaveDto = UserSaveDto.builder()
            .nip("222")
            .pesel("111")
            .email("info@info.pl")
            .password("pass")
            .addresses(List.of())
            .build();
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> userService.updateUser(id, userSaveDto));
        Mockito.verify(userRepository, Mockito.never()).existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip(), id);
        Mockito.verify(passwordEncoder, Mockito.never()).encode(userSaveDto.getPassword());
        Mockito.verify(userRepository, Mockito.never()).save(Mockito.any(User.class));
        Mockito.verify(userRepository).findById(id);
    }

    @Test
    void updateUserWithConflictExistingUser() {
        // given
        Integer id = 13;
        User user = User.builder()
            .id(id)
            .govData(GovData.builder()
                .nip("222")
                .pesel("111")
                .build())
            .email("info@info.pl")
            .password("pass")
            .build();
        UserSaveDto userSaveDto = UserSaveDto.builder()
            .nip("222")
            .pesel("111")
            .email("info@info.pl")
            .password("pass")
            .addresses(List.of())
            .build();
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip(), id)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> userService.updateUser(id, userSaveDto));
        Mockito.verify(userRepository).existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
            userSaveDto.getEmail(), userSaveDto.getPesel(), userSaveDto.getNip(), id);
        Mockito.verify(passwordEncoder, Mockito.never()).encode(userSaveDto.getPassword());
        Mockito.verify(userRepository, Mockito.never()).save(Mockito.any(User.class));
        Mockito.verify(userRepository).findById(id);
    }

    @Test
    void deleteUserExisting() {
        // given
        Integer id = 4;
        User user = User.builder().id(id).build();
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        // then
        userService.deleteUser(id);
    }

    @Test
    void deleteUserNonExisting() {
        // given
        Integer id = 4;
        // when
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> userService.deleteUser(id));
    }
}
