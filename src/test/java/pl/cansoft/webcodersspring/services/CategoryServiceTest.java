package pl.cansoft.webcodersspring.services;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.cansoft.webcodersspring.dtos.category.CategoryDto;
import pl.cansoft.webcodersspring.dtos.category.CategorySaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.repositories.CategoryRepository;

@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {

    @InjectMocks
    CategoryService categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Test
    void getCategories() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<Category> categoryList = List.of(
            Category.builder()
                .id(17)
                .products(List.of())
                .images(List.of())
                .build(),
            Category.builder()
                .id(18)
                .products(List.of())
                .images(List.of())
                .build(),
            Category.builder()
                .id(19)
                .products(List.of())
                .images(List.of())
                .build()
        );
        Page<Category> categoryPage = new PageImpl<>(categoryList);
        // when
        Mockito.when(categoryRepository.findAll(pageable)).thenReturn(categoryPage);
        Page<CategoryDto> response = categoryService.getCategories(pageable);
        // then
        Mockito.verify(categoryRepository).findAll(pageable);
        Assertions.assertEquals(3, response.getContent().size());
        Assertions.assertEquals(17, response.getContent().get(0).getId());
        Assertions.assertEquals(18, response.getContent().get(1).getId());
        Assertions.assertEquals(19, response.getContent().get(2).getId());
    }

    @Test
    void getCategoryWhenCategoryExists() {
        // given
        Integer id = 9;
        Optional<Category> optionalCategory = Optional.of(
            Category.builder()
                .id(id)
                .products(List.of())
                .images(List.of())
                .build()
        );
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        CategoryDto response = categoryService.getCategory(id);
        // then
        Mockito.verify(categoryRepository).findById(id);
        Assertions.assertEquals(id, response.getId());
    }

    @Test
    void getCategoryWhenCategoryDoesNotExist() {
        // given
        Integer id = 9;
        Optional<Category> optionalCategory = Optional.empty();
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> categoryService.getCategory(id));
        Mockito.verify(categoryRepository).findById(id);
    }

    @Test
    void addCategoryWhenCategoryExists() {
        // given
        String categoryName = "Phones";
        CategorySaveDto categorySaveDto = CategorySaveDto.builder()
            .name(categoryName)
            .build();
        // when
        Mockito.when(categoryRepository.existsByName(categoryName)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () ->
            categoryService.addCategory(categorySaveDto));
        Mockito.verify(categoryRepository).existsByName(categoryName);
    }

    @Test
    void addCategoryWhenCategoryDoesNotExist() {
        // given
        String categoryName = "Phones";
        Category category = Category.builder()
            .name(categoryName)
            .build();
        CategorySaveDto categorySaveDto = CategorySaveDto.builder()
            .name(categoryName)
            .images(List.of())
            .build();
        // when
        Mockito.when(categoryRepository.existsByName(categoryName)).thenReturn(false);
        categoryService.addCategory(categorySaveDto);
        // then
        Mockito.verify(categoryRepository).existsByName(categoryName);
        Mockito.verify(categoryRepository).save(category);
    }

    @Test
    void updateCategoryWhenCategoryExistsAndNameDoesNotExist() {
        // given
        Integer id = 14;
        String categoryName = "Phones";
        Category category = Category.builder()
            .id(id)
            .name(categoryName)
            .products(List.of())
            .images(List.of())
            .build();
        Optional<Category> optionalCategory = Optional.of(category);
        CategorySaveDto categorySaveDto = CategorySaveDto.builder()
            .name(categoryName)
            .images(List.of())
            .build();
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        Mockito.when(categoryRepository.existsByNameAndIdNot(categoryName, id)).thenReturn(false);
        categoryService.updateCategory(id, categorySaveDto);
        // then
        Mockito.verify(categoryRepository).findById(id);
        Mockito.verify(categoryRepository).existsByNameAndIdNot(categoryName, id);
        Mockito.verify(categoryRepository).save(category);
    }

    @Test
    void updateCategoryWhenCategoryExistsAndNameExists() {
        // given
        Integer id = 14;
        String categoryName = "Phones";
        Category category = Category.builder().build();
        Optional<Category> optionalCategory = Optional.of(category);
        CategorySaveDto categorySaveDto = CategorySaveDto.builder()
            .name(categoryName)
            .build();
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        Mockito.when(categoryRepository.existsByNameAndIdNot(categoryName, id)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> categoryService.updateCategory(id, categorySaveDto));
        Mockito.verify(categoryRepository).findById(id);
        Mockito.verify(categoryRepository).existsByNameAndIdNot(categoryName, id);
        Mockito.verify(categoryRepository, Mockito.never()).save(Mockito.any(Category.class));
    }

    @Test
    void updateCategoryWhenCategoryDoesNotExists() {
        // given
        Integer id = 14;
        Optional<Category> optionalCategory = Optional.empty();
        CategorySaveDto categorySaveDto = CategorySaveDto.builder().build();
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> categoryService.updateCategory(id, categorySaveDto));
        Mockito.verify(categoryRepository, Mockito.never()).save(Mockito.any(Category.class));
        Mockito.verify(categoryRepository).findById(id);
    }

    @Test
    void deleteCategoryWhenCategoryExists() {
        // given
        Integer id = 23;
        Category category = Category.builder().build();
        Optional<Category> optionalCategory = Optional.of(category);
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        categoryService.deleteCategory(id);
        // then
        Mockito.verify(categoryRepository).findById(id);
        Mockito.verify(categoryRepository).deleteById(id);
    }

    @Test
    void deleteCategoryWhenCategoryDoesNotExist() {
        // given
        Integer id = 23;
        Optional<Category> optionalCategory = Optional.empty();
        // when
        Mockito.when(categoryRepository.findById(id)).thenReturn(optionalCategory);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> categoryService.deleteCategory(id));
        Mockito.verify(categoryRepository).findById(id);
        Mockito.verify(categoryRepository, Mockito.never()).deleteById(id);
    }
}
