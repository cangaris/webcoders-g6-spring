package pl.cansoft.webcodersspring.services;

import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;
import pl.cansoft.webcodersspring.dtos.tag.TagSaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Tag;
import pl.cansoft.webcodersspring.repositories.TagRepository;

@ExtendWith(MockitoExtension.class)
class TagServiceTest {

    @InjectMocks
    TagService tagService;
    @Mock
    TagRepository tagRepository;

    @Test
    void getTags() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<Tag> tagList = List.of(Tag.builder().id(7).build());
        Page<Tag> tagPage = new PageImpl<>(tagList);
        // when
        Mockito.when(tagRepository.findAll(pageable)).thenReturn(tagPage);
        Page<TagDto> response = tagService.getTags(pageable);
        // then
        Mockito.verify(tagRepository).findAll(pageable);
        Assertions.assertEquals(1, response.getContent().size());
        Assertions.assertEquals(7, response.getContent().getFirst().getId());
    }

    @Test
    void insertTagWhenNameDoesNotExist() {
        // given
        String tagName = "Black Friday";
        Tag tag = Tag.builder()
            .name(tagName)
            .build();
        TagSaveDto tagSaveDto = TagSaveDto.builder()
            .name(tagName)
            .build();
        // when
        Mockito.when(tagRepository.existsByName(tagName)).thenReturn(false);
        tagService.insertTag(tagSaveDto);
        // then
        Mockito.verify(tagRepository).existsByName(tagName);
        Mockito.verify(tagRepository).save(tag);
    }

    @Test
    void insertTagWhenNameExists() {
        // given
        String tagName = "Black Friday";
        TagSaveDto tagSaveDto = TagSaveDto.builder()
            .name(tagName)
            .build();
        // when
        Mockito.when(tagRepository.existsByName(tagName)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> tagService.insertTag(tagSaveDto));
        Mockito.verify(tagRepository).existsByName(tagName);
        Mockito.verify(tagRepository, Mockito.never()).save(Mockito.any(Tag.class));
    }

    @Test
    void updateTagWhenNameDoesNotExist() {
        // given
        Integer id = 8;
        String tagName = "Black Friday";
        Tag tag = Tag.builder()
            .id(id)
            .name(tagName)
            .build();
        TagSaveDto tagSaveDto = TagSaveDto.builder()
            .name(tagName)
            .build();
        // when
        Mockito.when(tagRepository.existsByNameAndIdNot(tagName, id)).thenReturn(false);
        tagService.updateTag(id, tagSaveDto);
        // then
        Mockito.verify(tagRepository).existsByNameAndIdNot(tagName, id);
        Mockito.verify(tagRepository).save(tag);
    }

    @Test
    void updateTagWhenNameExists() {
        // given
        Integer id = 8;
        String tagName = "Black Friday";
        TagSaveDto tagSaveDto = TagSaveDto.builder()
            .name(tagName)
            .build();
        // when
        Mockito.when(tagRepository.existsByNameAndIdNot(tagName, id)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> tagService.updateTag(id, tagSaveDto));
        Mockito.verify(tagRepository).existsByNameAndIdNot(tagName, id);
        Mockito.verify(tagRepository, Mockito.never()).save(Mockito.any(Tag.class));
    }


    @Test
    void deleteTagWhenIdDoesNotExist() {
        // given
        Integer id = 19;
        // when
        Mockito.when(tagRepository.existsById(id)).thenReturn(false);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> tagService.deleteTag(id));
        Mockito.verify(tagRepository).existsById(id);
        Mockito.verify(tagRepository, Mockito.never()).deleteById(id);
    }

    @Test
    void deleteTagWhenIdExists() {
        // given
        Integer id = 19;
        // when
        Mockito.when(tagRepository.existsById(id)).thenReturn(true);
        tagService.deleteTag(id);
        // then
        Mockito.verify(tagRepository).existsById(id);
        Mockito.verify(tagRepository).deleteById(id);
    }
}
