package pl.cansoft.webcodersspring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.cansoft.webcodersspring.dtos.product.ProductDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSaveDto;
import pl.cansoft.webcodersspring.dtos.product.ProductTagDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Product;
import pl.cansoft.webcodersspring.models.Tag;
import pl.cansoft.webcodersspring.repositories.ProductRepository;
import pl.cansoft.webcodersspring.repositories.TagRepository;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @InjectMocks
    ProductService productService;

    @Mock
    ProductRepository productRepository;
    @Mock
    TagRepository tagRepository;

    @Test
    void getProducts() {
        // given
        Pageable pageable = PageRequest.of(0, 20);
        List<Product> productList = List.of(
            Product.builder()
                .id(17)
                .tags(List.of())
                .categories(List.of())
                .images(List.of())
                .build(),
            Product.builder()
                .id(18)
                .tags(List.of())
                .categories(List.of())
                .images(List.of())
                .build(),
            Product.builder()
                .id(19)
                .tags(List.of())
                .categories(List.of())
                .images(List.of())
                .build()
        );
        Page<Product> productPage = new PageImpl<>(productList);
        // when
        Mockito.when(productRepository.findAll(pageable)).thenReturn(productPage);
        Page<ProductDto> response = productService.getProducts(pageable);
        // then
        Mockito.verify(productRepository).findAll(pageable);
        Assertions.assertEquals(3, response.getContent().size());
        Assertions.assertEquals(17, response.getContent().get(0).getId());
        Assertions.assertEquals(18, response.getContent().get(1).getId());
        Assertions.assertEquals(19, response.getContent().get(2).getId());
    }

    @Test
    void getProductWhenProductExists() {
        // given
        Integer id = 9;
        Optional<Product> optionalProduct = Optional.of(
            Product.builder()
                .id(id)
                .tags(List.of())
                .categories(List.of())
                .images(List.of())
                .build()
        );
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        ProductDto response = productService.getProduct(id);
        // then
        Mockito.verify(productRepository).findById(id);
        Assertions.assertEquals(id, response.getId());
    }

    @Test
    void getProductWhenProductDoesNotExist() {
        // given
        Integer id = 9;
        Optional<Product> optionalProduct = Optional.empty();
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> productService.getProduct(id));
        Mockito.verify(productRepository).findById(id);
    }

    @Test
    void addProductWhenProductExists() {
        // given
        String productName = "iPhone";
        ProductSaveDto productSaveDto = ProductSaveDto.builder()
            .name(productName)
            .build();
        // when
        Mockito.when(productRepository.existsByName(productName)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () ->
            productService.addProduct(productSaveDto));
        Mockito.verify(productRepository).existsByName(productName);
    }

    @Test
    void addProductWhenProductDoesNotExist() {
        // given
        String productName = "iPhone";
        Product product = Product.builder()
            .name(productName)
            .build();
        ProductSaveDto productSaveDto = ProductSaveDto.builder()
            .name(productName)
            .images(List.of())
            .build();
        // when
        Mockito.when(productRepository.existsByName(productName)).thenReturn(false);
        productService.addProduct(productSaveDto);
        // then
        Mockito.verify(productRepository).existsByName(productName);
        Mockito.verify(productRepository).save(product);
    }

    @Test
    void updateProductWhenProductExistsAndNameDoesNotExist() {
        // given
        Integer id = 14;
        String productName = "iPhone";
        Product product = Product.builder()
            .id(id)
            .name(productName)
            .build();
        Optional<Product> optionalProduct = Optional.of(product);
        ProductSaveDto productSaveDto = ProductSaveDto.builder()
            .name(productName)
            .images(List.of())
            .build();
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        Mockito.when(productRepository.existsByNameAndIdNot(productName, id)).thenReturn(false);
        productService.updateProduct(id, productSaveDto);
        // then
        Mockito.verify(productRepository).findById(id);
        Mockito.verify(productRepository).existsByNameAndIdNot(productName, id);
        Mockito.verify(productRepository).save(product);
    }

    @Test
    void updateProductWhenProductExistsAndNameExists() {
        // given
        Integer id = 14;
        String productName = "iPhone";
        Product product = Product.builder().build();
        Optional<Product> optionalProduct = Optional.of(product);
        ProductSaveDto productSaveDto = ProductSaveDto.builder()
            .name(productName)
            .build();
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        Mockito.when(productRepository.existsByNameAndIdNot(productName, id)).thenReturn(true);
        // then
        Assertions.assertThrows(ConflictException.class, () -> productService.updateProduct(id, productSaveDto));
        Mockito.verify(productRepository).findById(id);
        Mockito.verify(productRepository).existsByNameAndIdNot(productName, id);
        Mockito.verify(productRepository, Mockito.never()).save(Mockito.any(Product.class));
    }

    @Test
    void updateProductWhenProductDoesNotExists() {
        // given
        Integer id = 14;
        Optional<Product> optionalProduct = Optional.empty();
        ProductSaveDto productSaveDto = ProductSaveDto.builder().build();
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> productService.updateProduct(id, productSaveDto));
        Mockito.verify(productRepository, Mockito.never()).save(Mockito.any(Product.class));
        Mockito.verify(productRepository, Mockito.never()).existsByNameAndIdNot(Mockito.anyString(), Mockito.anyInt());
        Mockito.verify(productRepository).findById(id);
    }

    @Test
    void deleteProductWhenProductExists() {
        // given
        Integer id = 23;
        Product product = Product.builder().build();
        Optional<Product> optionalProduct = Optional.of(product);
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        productService.deleteProduct(id);
        // then
        Mockito.verify(productRepository).findById(id);
        Mockito.verify(productRepository).deleteById(id);
    }

    @Test
    void deleteProductWhenProductDoesNotExist() {
        // given
        Integer id = 23;
        Optional<Product> optionalProduct = Optional.empty();
        // when
        Mockito.when(productRepository.findById(id)).thenReturn(optionalProduct);
        // then
        Assertions.assertThrows(NotFoundException.class, () -> productService.deleteProduct(id));
        Mockito.verify(productRepository).findById(id);
        Mockito.verify(productRepository, Mockito.never()).deleteById(id);
    }

    @Test
    void assignTagToProductWhenTagExistsAndProductExists() {
        // given
        ProductTagDto productTagDto = ProductTagDto.builder()
            .productId(5)
            .tagId(3)
            .build();
        Tag tag = Tag.builder().build();
        Product product = Product.builder()
            .tags(new ArrayList<>())
            .build();
        // when
        Mockito.when(tagRepository.findById(productTagDto.getTagId())).thenReturn(Optional.of(tag));
        Mockito.when(productRepository.findById(productTagDto.getProductId())).thenReturn(Optional.of(product));
        productService.assignTagToProduct(productTagDto);
        // then
        Mockito.verify(tagRepository).findById(productTagDto.getTagId());
        Mockito.verify(productRepository).findById(productTagDto.getProductId());
        Mockito.verify(productRepository).save(product);
    }

    @Test
    void assignTagToProductWhenTagExistsAndProductDoesNotExist() {
        // given
        ProductTagDto productTagDto = ProductTagDto.builder()
            .productId(5)
            .tagId(3)
            .build();
        Tag tag = Tag.builder().build();
        // when
        Mockito.when(tagRepository.findById(productTagDto.getTagId())).thenReturn(Optional.of(tag));
        Mockito.when(productRepository.findById(productTagDto.getProductId())).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> productService.assignTagToProduct(productTagDto));
        Mockito.verify(tagRepository).findById(productTagDto.getTagId());
        Mockito.verify(productRepository).findById(productTagDto.getProductId());
        Mockito.verify(productRepository, Mockito.never()).save(Mockito.any(Product.class));
    }

    @Test
    void assignTagToProductWhenTagDoesNotExistAndProductExists() {
        // given
        ProductTagDto productTagDto = ProductTagDto.builder()
            .productId(5)
            .tagId(3)
            .build();
        // when
        Mockito.when(tagRepository.findById(productTagDto.getTagId())).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> productService.assignTagToProduct(productTagDto));
        Mockito.verify(tagRepository).findById(productTagDto.getTagId());
        Mockito.verify(productRepository, Mockito.never()).findById(productTagDto.getProductId());
        Mockito.verify(productRepository, Mockito.never()).save(Mockito.any(Product.class));
    }
}
