package pl.cansoft.webcodersspring.services;

import java.util.ArrayList;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.webcodersspring.dtos.category_product.CategoryProductDto;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.models.Product;
import pl.cansoft.webcodersspring.repositories.CategoryRepository;
import pl.cansoft.webcodersspring.repositories.ProductRepository;

@ExtendWith(MockitoExtension.class)
class CategoryProductServiceTest {

    @InjectMocks
    CategoryProductService categoryProductService;

    @Mock
    ProductRepository productRepository;
    @Mock
    CategoryRepository categoryRepository;

    @Test
    void connectCategoryProduct() {
        // given
        Integer categoryId = 9;
        Integer productId = 8;
        Category category = Category.builder()
            .id(categoryId)
            .products(new ArrayList<>())
            .build();
        CategoryProductDto dto = CategoryProductDto.builder()
            .categoryId(categoryId)
            .productId(productId)
            .build();
        Product product = Product.builder().build();
        Optional<Product> optionalProduct = Optional.of(product);
        Optional<Category> optionalCategory = Optional.of(category);
        // when
        Mockito.when(productRepository.findById(dto.getProductId())).thenReturn(optionalProduct);
        Mockito.when(categoryRepository.findById(dto.getCategoryId())).thenReturn(optionalCategory);
        categoryProductService.connectCategoryProduct(dto);
        // then
        Mockito.verify(productRepository).findById(dto.getProductId());
        Mockito.verify(categoryRepository).findById(dto.getCategoryId());
        Mockito.verify(categoryRepository).save(category);
    }

    @Test
    void connectCategoryProductWhenProductDoesNotExist() {
        // given
        Integer categoryId = 9;
        Integer productId = 8;
        CategoryProductDto dto = CategoryProductDto.builder()
            .categoryId(categoryId)
            .productId(productId)
            .build();
        // when
        Mockito.when(productRepository.findById(dto.getProductId())).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> {
            categoryProductService.connectCategoryProduct(dto);
        });
        Mockito.verify(productRepository).findById(dto.getProductId());
        Mockito.verify(categoryRepository, Mockito.never()).findById(dto.getCategoryId());
        Mockito.verify(categoryRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void connectCategoryProductWhenCategoryDoesNotExist() {
        // given
        Integer categoryId = 9;
        Integer productId = 8;
        CategoryProductDto dto = CategoryProductDto.builder()
            .categoryId(categoryId)
            .productId(productId)
            .build();
        Product product = Product.builder().build();
        Optional<Product> optionalProduct = Optional.of(product);
        // when
        Mockito.when(productRepository.findById(dto.getProductId())).thenReturn(optionalProduct);
        Mockito.when(categoryRepository.findById(dto.getCategoryId())).thenReturn(Optional.empty());
        // then
        Assertions.assertThrows(NotFoundException.class, () -> {
            categoryProductService.connectCategoryProduct(dto);
        });
        Mockito.verify(productRepository).findById(dto.getProductId());
        Mockito.verify(categoryRepository).findById(dto.getCategoryId());
        Mockito.verify(categoryRepository, Mockito.never()).save(Mockito.any());
    }
}
