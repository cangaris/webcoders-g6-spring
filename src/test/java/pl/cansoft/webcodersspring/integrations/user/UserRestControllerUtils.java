package pl.cansoft.webcodersspring.integrations.user;

class UserRestControllerUtils {
    static final String ACCESS_DENIED_PAYLOAD = """
        [
            "Access denied for that operation"
        ]
        """;

    static final String NOT_FOUND = """
        [
            "User not found"
        ]
        """;

    static final String UPDATE_USER_PAYLOAD_9 = """
        {
            "firstName": "Piotr",
            "lastName": "Nowak",
            "email": "pio@now9.pl",
            "nip": "0000000009",
            "pesel": "00000000009",
            "password": "Pass11PASS",
            "role": "CLIENT",
            "phone": {
                "number": "666",
                "prefix": "+48"
            },
            "addresses": [{
                "city": "Kraków",
                "street": "Krakowska 3/3"
            }]
        }
        """;

    static final String UPDATE_USER_PAYLOAD_1 = """
        {
            "firstName": "Jan",
            "lastName": "Kowalski",
            "email": "pio@now1.pl",
            "nip": "0000000001",
            "pesel": "00000000001",
            "password": "Pass11PASS",
            "role": "CLIENT",
            "phone": {
                "number": "666",
                "prefix": "+48"
            },
            "addresses": [{
                "city": "Kraków",
                "street": "Krakowska 3/3"
            }]
        }
        """;

    static final String UPDATE_USER_PAYLOAD_2 = """
        {
            "firstName": "Piotr",
            "lastName": "Nowak",
            "email": "pio@now2.pl",
            "nip": "0000000002",
            "pesel": "00000000002",
            "password": "Pass11PASS",
            "role": "CLIENT",
            "phone": {
                "number": "666",
                "prefix": "+48"
            },
            "addresses": [{
                "city": "Kraków",
                "street": "Krakowska 3/3"
            }]
        }
        """;

    static final String INSERT_USER_PAYLOAD_1 = """
            {
                "firstName": "Jan",
                "lastName": "Kowalski",
                "email": "pio@now1.pl",
                "nip": "0000000001",
                "pesel": "00000000001",
                "password": "Pass11PASS",
                "role": "CLIENT",
                "phone": {
                    "number": "666",
                    "prefix": "+48"
                },
                "addresses": [{
                    "city": "Kraków",
                    "street": "Krakowska 3/3"
                }]
            }
        """;

    static final String INSERT_USER_PAYLOAD_2 = """
        {
            "firstName": "Jan",
            "lastName": "Kowalski",
            "email": "pio@now2.pl",
            "nip": "0000000002",
            "pesel": "00000000002",
            "password": "Pass11PASS",
            "role": "CLIENT",
            "phone": {
                "number": "666",
                "prefix": "+48"
            },
            "addresses": [{
                "city": "Kraków",
                "street": "Krakowska 3/3"
            }]
        }
        """;

    static final String ALL_USERS = """
        {
            "content": [
                {
                    "id": 1,
                    "firstName": "Jan",
                    "lastName": "Kowalski",
                    "email": "pio@now1.pl",
                    "role": "CLIENT",
                    "nip": "0000000001",
                    "phone": {
                        "id": 1,
                        "prefix": "+48",
                        "number": "666"
                    },
                    "addresses": [
                        {
                            "id": 1,
                            "city": "Kraków",
                            "street": "Krakowska 3/3"
                        }
                    ]
                },
                {
                    "id": 2,
                    "firstName": "Jan",
                    "lastName": "Kowalski",
                    "email": "pio@now2.pl",
                    "role": "CLIENT",
                    "nip": "0000000002",
                    "phone": {
                        "id": 2,
                        "prefix": "+48",
                        "number": "666"
                    },
                    "addresses": [
                        {
                            "id": 2,
                            "city": "Kraków",
                            "street": "Krakowska 3/3"
                        }
                    ]
                }
            ],
            "pageable": {
                "pageNumber": 0,
                "pageSize": 20,
                "sort": {
                    "empty": true,
                    "sorted": false,
                    "unsorted": true
                },
                "offset": 0,
                "paged": true,
                "unpaged": false
            },
            "last": true,
            "totalPages": 1,
            "totalElements": 2,
            "first": true,
            "size": 20,
            "number": 0,
            "sort": {
                "empty": true,
                "sorted": false,
                "unsorted": true
            },
            "numberOfElements": 2,
            "empty": false
        }
        """;

    static final String GET_USER_2 = """
        {
            "id": 2,
            "firstName": "Jan",
            "lastName": "Kowalski",
            "email": "pio@now2.pl",
            "role": "CLIENT",
            "nip": "0000000002",
            "phone": {
                "id": 2,
                "prefix": "+48",
                "number": "666"
            },
            "addresses": [
                {
                    "id": 2,
                    "city": "Kraków",
                    "street": "Krakowska 3/3"
                }
            ]
        }
        """;

    static final String GET_USER_1 = """
        {
            "id": 1,
            "firstName": "Jan",
            "lastName": "Kowalski",
            "email": "pio@now1.pl",
            "role": "CLIENT",
            "nip": "0000000001",
            "phone": {
                "id": 1,
                "prefix": "+48",
                "number": "666"
            },
            "addresses": [
                {
                    "id": 1,
                    "city": "Kraków",
                    "street": "Krakowska 3/3"
                }
            ]
        }
        """;

    static final String USER_INVALID_PAYLOAD = """
        {}
        """;

    static final String BAD_REQUEST_RESPONSE = """
        [
            "firstName must not be blank",
            "role must not be blank",
            "nip must not be blank",
            "lastName must not be blank",
            "password must not be blank",
            "pesel must not be blank"
        ]
        """;
}
