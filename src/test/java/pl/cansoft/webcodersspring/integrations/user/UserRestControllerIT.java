package pl.cansoft.webcodersspring.integrations.user;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest // podnosi caly context spinga ze wszystkimi realnymi beansami
@AutoConfigureMockMvc // konfiguracja narzedzia do zapytan http
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ActiveProfiles("test")
class UserRestControllerIT {

    static final String ADMIN = "ADMIN";
    static final String CLIENT = "CLIENT";

    static final Integer LOGGED_UID = 1;
    static final Integer DIFFERENT_EXISTING_UID = 2;
    static final Integer DIFFERENT_NON_EXISTING_UID = 3;

    @Autowired
    MockMvc mockMvc; // do zapytan http

    @Order(1)
    @Test
    void addUser_forInvalidPayload() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.USER_INVALID_PAYLOAD))
            .andExpect(MockMvcResultMatchers.status().isBadRequest())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.BAD_REQUEST_RESPONSE));
    }

    @Order(2)
    @Test
    void addUserLoggedUser_shouldEndWith204() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.INSERT_USER_PAYLOAD_1))
            .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Order(3)
    @Test
    void addUserDifferentUser_shouldEndWith204() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.INSERT_USER_PAYLOAD_2))
            .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Order(4)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl") // udajemy zalogowanego z rola CLIENT
    @Test
    void getUserLogged_whenClientRole_shouldReturnDataAndCode200() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/user/" + LOGGED_UID)) // wykonaj zapytanie
            .andExpect(MockMvcResultMatchers.status().isOk()) // sprawdz czy status 200
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.GET_USER_1));
        // sprawdz czy json zwrotki równy temu co podane wyżej
    }

    @Order(5)
    @WithMockUser(roles = ADMIN, username = "pio@now1.pl") // udajemy zalogowanego z rola CLIENT
    @Test
    void getUserNonExisting_shouldTrowsNotFoundException() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/user/" + DIFFERENT_NON_EXISTING_UID)) // wykonaj zapytanie
            .andExpect(MockMvcResultMatchers.status().isNotFound()) // sprawdz status
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.NOT_FOUND));
        // sprawdz czy message bledu jest zgodny z oczekiwanym
    }

    @Order(6)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl") // udajemy zalogowanego z rola CLIENT
    @Test
    void getUserDiffExisting_shouldTrowsForbiddenException() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/user/" + DIFFERENT_EXISTING_UID)) // wykonaj zapytanie
            .andExpect(MockMvcResultMatchers.status().isForbidden()) // sprawdz status
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.ACCESS_DENIED_PAYLOAD));
        // sprawdz czy message bledu jest zgodny z oczekiwanym
    }

    @Order(7)
    @WithMockUser(roles = ADMIN, username = "pio@now1.pl") // udajemy zalogowanego z rola ADMIN
    @Test
    void getUserDiffExisting_whenAdminRole_shouldReturnDataAndCode200() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/user/" + DIFFERENT_EXISTING_UID)) // wykonaj zapytanie
            .andExpect(MockMvcResultMatchers.status().isOk()) // sprawdz status
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.GET_USER_2));
    }

    @Order(8)
    @WithMockUser(roles = ADMIN)
    @Test
    void getUsers_shouldEndWith200_withAdminRole() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.ALL_USERS));
    }

    @Order(9)
    @WithMockUser(roles = CLIENT)
    @Test
    void getUsers_shouldEndWith403_withClientRole() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user"))
            .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Order(10)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl")
    @Test
    void updateUser_shouldEndWith204() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/user/" + LOGGED_UID)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.UPDATE_USER_PAYLOAD_1))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Order(11)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl")
    @Test
    void updateUser_forInvalidPayload() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/user/" + LOGGED_UID)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.USER_INVALID_PAYLOAD))
            .andExpect(MockMvcResultMatchers.status().isBadRequest())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.BAD_REQUEST_RESPONSE));
    }

    @Order(12)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl")
    @Test
    void updateUser_shouldFail_forClientRole_forDiffUserExisting() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/user/" + DIFFERENT_EXISTING_UID)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.UPDATE_USER_PAYLOAD_9))
            .andExpect(MockMvcResultMatchers.status().isForbidden())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.ACCESS_DENIED_PAYLOAD));
    }

    @Order(13)
    @WithMockUser(roles = ADMIN, username = "pio@now1.pl")
    @Test
    void updateUser_shouldWork_forAdminRole_forDiffUserExisting() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/user/" + DIFFERENT_EXISTING_UID)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.UPDATE_USER_PAYLOAD_2))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Order(14)
    @WithMockUser(roles = ADMIN, username = "pio@now1.pl")
    @Test
    void updateUser_shouldFail_forAdminRole_forUserNotExising() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.put("/user/" + DIFFERENT_NON_EXISTING_UID)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UserRestControllerUtils.UPDATE_USER_PAYLOAD_9))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.NOT_FOUND));
    }

    @Order(15)
    @WithMockUser(roles = ADMIN) // username = "pio@now1.pl"
    @Test
    void deleteUser_whenAdminRole_forNotExistingUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + DIFFERENT_NON_EXISTING_UID))
            .andExpect(MockMvcResultMatchers.status().isNotFound())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.NOT_FOUND));
    }

    @Order(16)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl")
    @Test
    void deleteUser_whenClientRole_forDiffExistingUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + DIFFERENT_EXISTING_UID))
            .andExpect(MockMvcResultMatchers.status().isForbidden())
            .andExpect(MockMvcResultMatchers.content().json(UserRestControllerUtils.ACCESS_DENIED_PAYLOAD));
    }

    @Order(17)
    @WithMockUser(roles = ADMIN) // username = "pio@now1.pl"
    @Test
    void deleteUser_whenAdminRole_forDiffExistingUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + DIFFERENT_EXISTING_UID))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Order(18)
    @WithMockUser(roles = CLIENT, username = "pio@now1.pl")
    @Test
    void deleteUser_whenClientRole_forLoggedUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + LOGGED_UID))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
