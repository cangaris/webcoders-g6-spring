CREATE TABLE app_user
(
    id          INT AUTO_INCREMENT NOT NULL,
    first_name  VARCHAR(50)        NOT NULL,
    last_name   VARCHAR(50)        NOT NULL,
    email       VARCHAR(100)       NOT NULL,
    password    VARCHAR(255)       NOT NULL,
    `role`      VARCHAR(10)        NOT NULL,
    gov_data_id INT                NULL,
    phone_id    INT                NULL,
    CONSTRAINT pk_appuser PRIMARY KEY (id)
);

