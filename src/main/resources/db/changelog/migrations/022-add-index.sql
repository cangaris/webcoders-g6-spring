ALTER TABLE app_user
    ADD CONSTRAINT uc_appuser_email UNIQUE (email);
