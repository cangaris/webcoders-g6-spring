CREATE TABLE phone
(
    id     INT AUTO_INCREMENT NOT NULL,
    prefix VARCHAR(255)       NULL,
    number VARCHAR(255)       NULL,
    CONSTRAINT pk_phone PRIMARY KEY (id)
);
