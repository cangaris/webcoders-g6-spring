ALTER TABLE category_product_pivot
    ADD CONSTRAINT fk_catpropiv_on_product FOREIGN KEY (product_id) REFERENCES product (id);
