CREATE TABLE category
(
    id      INT AUTO_INCREMENT NOT NULL,
    name    VARCHAR(255)       NOT NULL,
    content VARCHAR(2000)      NULL,
    CONSTRAINT pk_category PRIMARY KEY (id)
);
