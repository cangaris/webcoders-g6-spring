CREATE TABLE product
(
    id      INT AUTO_INCREMENT NOT NULL,
    name    VARCHAR(255)       NOT NULL,
    content VARCHAR(2000)      NULL,
    price   DECIMAL            NULL,
    CONSTRAINT pk_product PRIMARY KEY (id)
);

