CREATE TABLE gov_data
(
    id    INT AUTO_INCREMENT NOT NULL,
    pesel VARCHAR(11)        NULL,
    nip   VARCHAR(10)        NULL,
    CONSTRAINT pk_govdata PRIMARY KEY (id)
);
