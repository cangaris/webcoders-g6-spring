ALTER TABLE product_tag_pivot
    ADD CONSTRAINT fk_protagpiv_on_product FOREIGN KEY (product_id) REFERENCES product (id);
