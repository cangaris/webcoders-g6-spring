CREATE TABLE address
(
    id      INT AUTO_INCREMENT NOT NULL,
    city    VARCHAR(100)       NOT NULL,
    street  VARCHAR(100)       NOT NULL,
    user_id INT                NULL,
    CONSTRAINT pk_address PRIMARY KEY (id)
);


