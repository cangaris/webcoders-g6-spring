ALTER TABLE product_tag_pivot
    ADD CONSTRAINT fk_protagpiv_on_tag FOREIGN KEY (tag_id) REFERENCES tag (id);
