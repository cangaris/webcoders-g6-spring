package pl.cansoft.webcodersspring.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.address.AddressSaveDto;
import pl.cansoft.webcodersspring.services.AddressService;
import pl.cansoft.webcodersspring.utils.AppUtils;

@RestController // tworzy bean'a
@RequiredArgsConstructor
public class AddressRestController {

    private final AddressService addressService;

    @Secured(AppUtils.ROLE_ADMIN)
    @GetMapping("/address")
    ResponseEntity<Page<AddressDto>> getAddresses(Pageable pageable) {
        Page<AddressDto> addressDtos = addressService.getAddresses(pageable);
        return ResponseEntity.ok(addressDtos);
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @GetMapping("/address/{id}")
    ResponseEntity<AddressDto> getAddress(@PathVariable Integer id) {
        AddressDto addressDto = addressService.getAddress(id);
        return ResponseEntity.ok(addressDto);
    }

    /*
      {
      "city": "Warszawa",
      "street": "Marszałkowska 10"
      }
     */
    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("/address")
    ResponseEntity<Void> insertAddress(@RequestBody @Valid AddressSaveDto addressDto) {
        addressService.insertAddress(addressDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @PutMapping("/address/{id}")
    ResponseEntity<Void> updateAddress(@PathVariable Integer id, @RequestBody @Valid AddressSaveDto addressDto) {
        addressService.updateAddress(id, addressDto);
        return ResponseEntity.noContent().build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @DeleteMapping("/address/{id}")
    ResponseEntity<Void> deleteAddress(@PathVariable Integer id) {
        addressService.deleteAddress(id);
        return ResponseEntity.noContent().build();
    }
}
