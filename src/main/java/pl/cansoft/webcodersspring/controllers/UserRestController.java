package pl.cansoft.webcodersspring.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.user.UserDto;
import pl.cansoft.webcodersspring.dtos.user.UserSaveDto;
import pl.cansoft.webcodersspring.services.UserService;
import pl.cansoft.webcodersspring.utils.AppUtils;

// JPA - Java Persistence Api, Data - dane
// Hibernate implementuje JPA - jest ORMem - będzie pomagał w zapytaniach SQL
// ORM - Object-Relational Mapping (tworzy relacje pomiędzy obiektami Java a tabelami w bazie danych)

// [GET] localhost:8080/user - get all
// [POST] localhost:8080/user - add new
// [GET] localhost:8080/user/1 - get one by id
// [PUT] localhost:8080/user/1 - update one by id
// [DELETE] localhost:8080/user/1 - delete one

/**
 * CRUD - create, read, update, delete
 * [GET] - można pobierać dane (już istniejące wcześniej)
 * [POST] - dodawać dane
 * [PUT][PATCH] - aktualizować dane
 * [DELETE] - usuwać dane
 */
@RestController // Bean
@RequiredArgsConstructor // wygeneruje konstruktor dla wszystkich pol finalnych
public class UserRestController {

    /**
     * na start spring skaluje projekt i tworzy beany (IoC)
     * potem beany są dostępne dla usera i możliwe do wstrzykiwania (DI) poprzez konstruktor
     * DI - Dependency Injection
     * IoC - Inversion of Control
     */
    private final UserService userService; // dostarczony bean przez @RequiredArgsConstructor

    // [GET] localhost:8080/user
    @Secured(AppUtils.ROLE_ADMIN)
    @GetMapping("user") // [GET] /user
    public ResponseEntity<Page<UserDto>> getUsers(Pageable pageable) {
        Page<UserDto> usersDto = userService.getUsers(pageable);
        return ResponseEntity.ok(usersDto);
    }

    // [GET] localhost:8080/user?index=1 -> (@RequestParam) query string (często do parametrów nie wymaganych np. wyszukiwarka)
    // [GET] localhost:8080/user/1 -> (@PathVariable) path variable (lepsze do parametru wymaganego)
    @Secured({AppUtils.ROLE_CLIENT, AppUtils.ROLE_ADMIN})
    @GetMapping("user/{id}") // GET /user/1
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer id) {
        UserDto userDto = userService.getUser(id);
        return ResponseEntity.ok(userDto);
    }

    /* DTO - data transfer object
        {
            "firstName": "Piotr",
            "lastName": "Nowak",
            "email": "pio@now4.pl",
            "nip": "2222222223",
            "pesel": "11111111112",
            "password": "Pass11PASS",
            "role": "CLIENT",
            "phone": {
                "number": "666",
                "prefix": "+48"
            },
            "addresses": [{
                "city": "Kraków",
                "street": "Krakowska 3/3"
            }]
        }
    */
    @PostMapping("user") // [POST] /user
    public ResponseEntity<Void> addNewUser(@RequestBody @Valid UserSaveDto userDto) {
        userService.insertUser(userDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Secured({AppUtils.ROLE_CLIENT, AppUtils.ROLE_ADMIN})
    @PutMapping("user/{id}") // [PUT] /user/1
    public ResponseEntity<Void> editUser(@PathVariable Integer id, @RequestBody @Valid UserSaveDto userDto) {
        userService.updateUser(id, userDto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Secured({AppUtils.ROLE_CLIENT, AppUtils.ROLE_ADMIN})
    @DeleteMapping("user/{id}") // [DELETE] /user/1
    public ResponseEntity<Void> removeUser(@PathVariable Integer id) {
        userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
