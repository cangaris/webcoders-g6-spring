package pl.cansoft.webcodersspring.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.product.ProductDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSaveDto;
import pl.cansoft.webcodersspring.dtos.product.ProductTagDto;
import pl.cansoft.webcodersspring.services.ProductService;
import pl.cansoft.webcodersspring.utils.AppUtils;

@RestController
@RequiredArgsConstructor // konstruktor dla wszystkich pól finalnych
public class ProductRestController {

    private final ProductService productService;

    /*
    List -> Page
    localhost:8080/product?page=0&size=20
     */
    @GetMapping("product")
    public ResponseEntity<Page<ProductDto>> getProducts(Pageable pageable) { // @PageableDefault(size = 100)
        Page<ProductDto> productDtos = productService.getProducts(pageable);
        return ResponseEntity.ok(productDtos); // ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("product/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable Integer id) {
        ProductDto productDto = productService.getProduct(id);
        return ResponseEntity.ok(productDto);
    }

    /*
    {
    "tagId": 1,
    "productId": 1
    }
     */
    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("product/tag")
    public ResponseEntity<Void> assignTagToProduct(@RequestBody ProductTagDto dto) {
        productService.assignTagToProduct(dto);
        return ResponseEntity.noContent().build();
    }

    /*
        {
        "name": "iPhone 13",
        "content": "opis",
        "price": 100,
        "images": [{ "uri": "..." }]
        }
     */
    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("product")
    public ResponseEntity<Void> addProduct(@RequestBody @Valid ProductSaveDto productDto) {
        productService.addProduct(productDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @PutMapping("product/{id}")
    public ResponseEntity<Void> updateProduct(@PathVariable Integer id, @RequestBody @Valid ProductSaveDto productDto) {
        productService.updateProduct(id, productDto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @DeleteMapping("product/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
}
