package pl.cansoft.webcodersspring.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.category.CategoryDto;
import pl.cansoft.webcodersspring.dtos.category.CategorySaveDto;
import pl.cansoft.webcodersspring.services.CategoryService;
import pl.cansoft.webcodersspring.utils.AppUtils;

@RestController
@RequiredArgsConstructor
public class CategoryRestController {

    private final CategoryService categoryService;

    @GetMapping("/category")
    ResponseEntity<Page<CategoryDto>> getCategories(Pageable pageable) {
        Page<CategoryDto> categoryDtos = categoryService.getCategories(pageable);
        return ResponseEntity.ok(categoryDtos);
    }

    @GetMapping("/category/{id}")
    ResponseEntity<CategoryDto> getCategory(@PathVariable Integer id) {
        CategoryDto categoryDto = categoryService.getCategory(id);
        return ResponseEntity.ok(categoryDto); // 200 z jakimś body
    }

    /* JACKSON - json <-> klasy Javowe
      {
      "name": "Servers",
      "content": "the best Servers",
      "images": [{ "uri": "..111." }]
      }
     */
    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("/category")
    ResponseEntity<Void> addCategory(@RequestBody @Valid CategorySaveDto categoryDto) {
        categoryService.addCategory(categoryDto);
        return ResponseEntity.status(HttpStatus.CREATED).build(); // 201
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @PutMapping("/category/{id}")
    ResponseEntity<Void> updateCategory(@PathVariable Integer id, @RequestBody @Valid CategorySaveDto categoryDto) {
        categoryService.updateCategory(id, categoryDto);
        return ResponseEntity.noContent().build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @DeleteMapping("/category/{id}")
    ResponseEntity<Void> deleteCategory(@PathVariable Integer id) {
        categoryService.deleteCategory(id);
        return ResponseEntity.noContent().build(); // 204 - sukces bez body
    }
}
