package pl.cansoft.webcodersspring.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;
import pl.cansoft.webcodersspring.dtos.tag.TagSaveDto;
import pl.cansoft.webcodersspring.services.TagService;
import pl.cansoft.webcodersspring.utils.AppUtils;

@RestController
@RequiredArgsConstructor
public class TagRestController {

    private final TagService tagService;

    @Secured(AppUtils.ROLE_ADMIN)
    @GetMapping("/tag")
    ResponseEntity<Page<TagDto>> getTags(Pageable pageable) {
        Page<TagDto> tagDtos = tagService.getTags(pageable);
        return ResponseEntity.ok(tagDtos);
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("/tag")
    ResponseEntity<Void> insertTag(@RequestBody @Valid TagSaveDto dto) {
        tagService.insertTag(dto);
        return ResponseEntity.noContent().build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @PutMapping("/tag/{id}")
    ResponseEntity<Void> updateTag(@PathVariable Integer id, @RequestBody @Valid TagSaveDto dto) {
        tagService.updateTag(id, dto);
        return ResponseEntity.noContent().build();
    }

    @Secured(AppUtils.ROLE_ADMIN)
    @DeleteMapping("/tag/{id}")
    ResponseEntity<Void> deleteTag(@PathVariable Integer id) {
        tagService.deleteTag(id);
        return ResponseEntity.noContent().build();
    }
}
