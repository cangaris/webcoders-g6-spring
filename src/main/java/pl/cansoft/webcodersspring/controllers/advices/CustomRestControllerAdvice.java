package pl.cansoft.webcodersspring.controllers.advices;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.cansoft.webcodersspring.exceptions.AccessDeniedException;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;

@RestControllerAdvice
public class CustomRestControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<List<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        List<String> error = exception.getFieldErrors()
            .stream()
            .map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage())
            .collect(Collectors.toList());
        exception.printStackTrace();
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(ConflictException.class)
    ResponseEntity<List<String>> handleConflictException(ConflictException exception) {
        exception.printStackTrace();
        List<String> errors = List.of(exception.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);
    }

    @ExceptionHandler(NotFoundException.class)
    ResponseEntity<List<String>> handleNotFoundException(NotFoundException exception) {
        exception.printStackTrace();
        List<String> errors = List.of(exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errors);
    }

    @ExceptionHandler(AccessDeniedException.class)
    ResponseEntity<List<String>> accessDeniedException(AccessDeniedException exception) {
        exception.printStackTrace();
        List<String> errors = List.of(exception.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errors);
    }
}
