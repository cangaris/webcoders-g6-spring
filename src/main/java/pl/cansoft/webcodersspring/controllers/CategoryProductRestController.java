package pl.cansoft.webcodersspring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.webcodersspring.dtos.category_product.CategoryProductDto;
import pl.cansoft.webcodersspring.services.CategoryProductService;
import pl.cansoft.webcodersspring.utils.AppUtils;

@RestController
@RequiredArgsConstructor
public class CategoryProductRestController {

    private final CategoryProductService categoryProductService;

    /*
     {
        "categoryId": 1,
        "productId": 1
     }
     */
    @Secured(AppUtils.ROLE_ADMIN)
    @PostMapping("/category-product")
    public ResponseEntity<Void> connectCategoryProduct(@RequestBody CategoryProductDto dto) {
        categoryProductService.connectCategoryProduct(dto);
        return ResponseEntity.noContent().build();
    }
}
