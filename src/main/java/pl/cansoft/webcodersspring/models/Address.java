package pl.cansoft.webcodersspring.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(of = {"id"}) // porownanie obiektow tylko po ID
@AllArgsConstructor // dodane konstruktory bo były wymagane przel builder
@NoArgsConstructor // dodane konstruktory bo były wymagane przel builder
@Builder // dodaje builder do klasy
@ToString
@Getter
@Setter
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 100, nullable = false)
    private String city;

    @Column(length = 100, nullable = false) // definicja kolumny w bazie
    private String street;
}
