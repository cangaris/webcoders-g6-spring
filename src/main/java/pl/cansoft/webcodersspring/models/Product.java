package pl.cansoft.webcodersspring.models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(of = {"id"})
@Builder
@Getter // wygeneruje gettery
@Setter // wygeneruje settery
@ToString // wygenetruje metode toString()
@NoArgsConstructor // wygeneruje konstruktor bez argumentów Product()
// wygeneruje konstruktor z wszystkimi argumentami
// Product(Integer id, String name, String content, BigDecimal price)
@AllArgsConstructor
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(length = 2000)
    private String content;

    private BigDecimal price;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "product_id") // unikniemy dodatkowej tabeli łączącel product-image
    private List<Image> images;

    // wykluczenie tego propsa z metody toString (inaczej mógłby być błąd nieskończonego odwołania w logach)
    @ToString.Exclude
    @ManyToMany(mappedBy = "products")
    private List<Category> categories;

    @ManyToMany
    @JoinTable(
        name = "product_tag_pivot",
        joinColumns = @JoinColumn(name = "product_id"),
        inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags;
}
