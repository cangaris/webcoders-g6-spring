package pl.cansoft.webcodersspring.models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*
 nazwa tabeli User jest zablokowana dla bazy H2
 w mysql ten problem nie występuje, póki korzystamy z H2 zmieniamy nazwę
 z User na AppUser za pomocą (name = "AppUser")
 */
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@Entity(name = "AppUser")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String firstName;

    @Column(nullable = false, length = 50)
    private String lastName;

    @Column(unique = true, nullable = false, length = 100)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 10)
    private String role;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true) // tabela A(FK) łączy się z B za pomocą PK i FK
    private GovData govData;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Phone phone;

    @ToString.Exclude
    // wykluczenie tego propsa z metody toString (inaczej mógłby być błąd nieskończonego odwołania w logach)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    // - tabela A może łączyć się z B za pomocą tabeli C(FK,FK), ale może też być połaczenie A z B(FK)
    @JoinColumn(name = "user_id") // przypadek nr 2 z powyższego komentarza
    private List<Address> addresses;
}
