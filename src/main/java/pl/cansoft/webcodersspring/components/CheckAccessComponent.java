package pl.cansoft.webcodersspring.components;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.cansoft.webcodersspring.exceptions.AccessDeniedException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.repositories.UserRepository;
import pl.cansoft.webcodersspring.utils.AppUtils;

@Component // powstaje z tej klasy bean i można go wstrzykiwać
@RequiredArgsConstructor
public class CheckAccessComponent {

    private final UserRepository userRepository;

    public void checkLoggedUserHasAccessToResources(Integer requestId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String principalName = authentication.getName();
        String userRole = authentication.getAuthorities().stream()
            .findFirst()
            .map(GrantedAuthority::getAuthority)
            .orElseThrow();
        if (!AppUtils.ROLE_ADMIN.equals(userRole)) {
            this.userRepository.findByEmail(principalName)
                .ifPresentOrElse(
                    loggedUser -> {
                        if (!requestId.equals(loggedUser.getId())) {
                            throw new AccessDeniedException();
                        }
                    }, () -> {
                        throw new NotFoundException("User not found");
                    }
                );
        }
    }
}
