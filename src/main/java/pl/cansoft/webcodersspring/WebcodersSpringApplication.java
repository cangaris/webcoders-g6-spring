package pl.cansoft.webcodersspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebcodersSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebcodersSpringApplication.class, args);
    }

}

