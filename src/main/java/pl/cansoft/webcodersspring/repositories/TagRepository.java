package pl.cansoft.webcodersspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.webcodersspring.models.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, Integer id);
}
