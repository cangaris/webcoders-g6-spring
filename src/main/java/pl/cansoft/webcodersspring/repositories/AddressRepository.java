package pl.cansoft.webcodersspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.webcodersspring.models.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
