package pl.cansoft.webcodersspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.webcodersspring.models.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    /**
     * select count(id) > 0 from Product where name = ?
     */
    boolean existsByName(String name);

    /**
     * select count(id) > 0 from Product where name = ? and id <> ?
     */
    boolean existsByNameAndIdNot(String name, Integer id);
}
