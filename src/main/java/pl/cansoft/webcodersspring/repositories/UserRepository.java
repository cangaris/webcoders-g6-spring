package pl.cansoft.webcodersspring.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.cansoft.webcodersspring.models.User;

// dziedziczenie po JpaRepository stworzy nam z repozytorium Bean'a
public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * select count(id) > 0 from User where name = ?
     */
    boolean existsByEmailOrGovDataPeselOrGovDataNip(String email, String pesel,
                                                    String nip); // gdy interesuje mnie czy element istnieje czy nie

    @Query("""
        select count(u) > 0 from AppUser u
        where (u.email = ?1 or u.govData.pesel = ?2 or u.govData.nip = ?3)
        and u.id <> ?4
        """)
        // HQL - hibernate query language, JPQL - java persistance query language
    boolean existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(String email, String pesel,
                                                            String nip, Integer id);

    /**
     * select count(id) > 0 from User where name = ? and id <> ?
     */
    boolean existsByEmailAndIdNot(String email, Integer id);

    // select count(id) > 0 from User where email = ?
    // int countByEmail(String email); // gdy interesuje mnie liczba danych elementow
    // select * from User where email = ?
    // @Query("select u from AppUser u where u.email = ?")
    Optional<User> findByEmail(String email);
    // select * from User where email = ?
    // Optional<User> findByEmail(String email); // absolutnie pewien ze jest tylko 1 wynik
}
