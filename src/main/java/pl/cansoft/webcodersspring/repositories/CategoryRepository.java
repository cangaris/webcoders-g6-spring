package pl.cansoft.webcodersspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.webcodersspring.models.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    /**
     * select count(id) > 0 from Category where name = ?
     */
    boolean existsByName(String name);

    /**
     * select count(id) > 0 from Category where name = ? and id <> ?
     */
    boolean existsByNameAndIdNot(String name, Integer id);
}
