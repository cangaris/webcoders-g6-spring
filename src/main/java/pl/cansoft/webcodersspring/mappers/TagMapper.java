package pl.cansoft.webcodersspring.mappers;

import org.springframework.data.domain.Page;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;
import pl.cansoft.webcodersspring.dtos.tag.TagSaveDto;
import pl.cansoft.webcodersspring.models.Tag;

public class TagMapper {

    public static Tag map(TagSaveDto tag) {
        return Tag.builder()
            .name(tag.getName())
            .build();
    }

    public static TagDto map(Tag tag) {
        return TagDto.builder()
            .id(tag.getId())
            .name(tag.getName())
            .build();
    }

    public static Page<TagDto> mapListDto(Page<Tag> tags) {
        return tags.map(TagMapper::map);
    }
}
