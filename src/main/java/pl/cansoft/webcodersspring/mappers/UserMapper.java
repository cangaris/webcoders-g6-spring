package pl.cansoft.webcodersspring.mappers;

import java.util.List;
import java.util.Objects;
import org.springframework.data.domain.Page;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.phone.PhoneDto;
import pl.cansoft.webcodersspring.dtos.phone.PhoneSaveDto;
import pl.cansoft.webcodersspring.dtos.user.UserDto;
import pl.cansoft.webcodersspring.dtos.user.UserSaveDto;
import pl.cansoft.webcodersspring.models.Address;
import pl.cansoft.webcodersspring.models.GovData;
import pl.cansoft.webcodersspring.models.Phone;
import pl.cansoft.webcodersspring.models.User;

public class UserMapper {

    // insert
    public static User map(UserSaveDto userDto) {
        Phone phone = mapPhone(userDto.getPhone());
        List<Address> addresses = AddressMapper.mapListToEntity(userDto.getAddresses());
        GovData govData = GovData.builder()
            .pesel(userDto.getPesel())
            .nip(userDto.getNip())
            .build();
        return User.builder()
            .email(userDto.getEmail())
            .firstName(userDto.getFirstName())
            .lastName(userDto.getLastName())
            .password(userDto.getPassword())
            .role(userDto.getRole())
            .govData(govData)
            .addresses(addresses)
            .phone(phone)
            .build();
    }

    // update
    public static User map(UserSaveDto userDto, User currentUser) {
        Phone phone = mapPhone(userDto.getPhone());
        List<Address> addresses = AddressMapper.mapListToEntity(userDto.getAddresses());
        GovData govData = GovData.builder()
            .id(currentUser.getGovData().getId())
            .pesel(userDto.getPesel())
            .nip(userDto.getNip())
            .build();
        return User.builder()
            .id(currentUser.getId())
            .email(userDto.getEmail())
            .firstName(userDto.getFirstName())
            .lastName(userDto.getLastName())
            .password(userDto.getPassword())
            .role(userDto.getRole())
            .govData(govData)
            .addresses(addresses)
            .phone(phone)
            .build();
    }

    public static UserDto map(User user) {
        List<Address> addresses = user.getAddresses();
        List<AddressDto> addressesDto = AddressMapper.map(addresses);
        PhoneDto phoneDto = mapPhone(user.getPhone());
        return UserDto.builder()
            .id(user.getId())
            .email(user.getEmail())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .role(user.getRole())
            .nip(user.getGovData().getNip())
            .addresses(addressesDto)
            .phone(phoneDto)
            .build();
    }

    public static Page<UserDto> map(Page<User> users) {
        return users.map(UserMapper::map);
    }

    public static PhoneDto mapPhone(Phone phone) {
//        return Optional.ofNullable(phone)
//            .map(ph -> PhoneDto.builder()
//                .number(ph.getNumber())
//                .number(ph.getPrefix())
//                .build()) // nie null
//            .orElse(null); // gdyby null
        return Objects.isNull(phone) ? null : PhoneDto.builder()
            .id(phone.getId())
            .number(phone.getNumber())
            .prefix(phone.getPrefix())
            .build();
    }

    public static Phone mapPhone(PhoneSaveDto phone) {
        return Objects.isNull(phone) ? null : Phone.builder()
            .number(phone.getNumber())
            .prefix(phone.getPrefix())
            .build();
    }
}
