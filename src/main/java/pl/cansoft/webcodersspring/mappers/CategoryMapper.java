package pl.cansoft.webcodersspring.mappers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import pl.cansoft.webcodersspring.dtos.category.CategoryDto;
import pl.cansoft.webcodersspring.dtos.category.CategorySaveDto;
import pl.cansoft.webcodersspring.dtos.category.CategorySimpleDto;
import pl.cansoft.webcodersspring.dtos.image.ImageDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSimpleDto;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.models.Image;
import pl.cansoft.webcodersspring.models.Product;

public class CategoryMapper {

    public static Category map(CategorySaveDto categoryDto) {
        List<Image> images = ImageMapper.mapImagesDto(categoryDto.getImages());
        return Category.builder()
            .name(categoryDto.getName())
            .content(categoryDto.getContent())
            .images(images)
            .build();
    }

    public static CategoryDto map(Category category) {
        List<Product> products = category.getProducts();
        List<ProductSimpleDto> productDtos = ProductMapper.mapListToSimpleDto(products);
        List<ImageDto> imageDtos = ImageMapper.mapImages(category.getImages());
        return CategoryDto.builder()
            .id(category.getId())
            .name(category.getName())
            .content(category.getContent())
            .products(productDtos)
            .images(imageDtos)
            .build();
    }

    public static CategorySimpleDto mapToSimpleDto(Category category) {
        return CategorySimpleDto.builder()
            .id(category.getId())
            .name(category.getName())
            .content(category.getContent())
            .build();
    }

    public static Page<CategoryDto> map(Page<Category> categories) {
        return categories.map(CategoryMapper::map);
    }

    public static List<CategorySimpleDto> mapListToSimpleDto(List<Category> categories) {
        return categories.stream()
            .map(category -> mapToSimpleDto(category))
            .collect(Collectors.toList());
    }
}
