package pl.cansoft.webcodersspring.mappers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.address.AddressSaveDto;
import pl.cansoft.webcodersspring.models.Address;

public class AddressMapper {

    public static Address map(AddressSaveDto addressDto) {
        return Address.builder()
            .street(addressDto.getStreet())
            .city(addressDto.getCity())
            .build();
    }

    public static AddressDto map(Address address) {
        return AddressDto.builder()
            .id(address.getId())
            .city(address.getCity())
            .street(address.getStreet())
            .build();
    }

    public static List<AddressDto> map(List<Address> addresses) {
        return addresses.stream()
            .map(AddressMapper::map)
            .collect(Collectors.toList());
    }

    public static Page<AddressDto> map(Page<Address> addresses) {
        return addresses.map(AddressMapper::map);
    }

    public static List<Address> mapListToEntity(List<AddressSaveDto> addresses) { // todo: name
        return addresses.stream()
            .map(AddressMapper::map)
            .collect(Collectors.toList());
    }
}
