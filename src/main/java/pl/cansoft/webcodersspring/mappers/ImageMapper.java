package pl.cansoft.webcodersspring.mappers;

import java.util.List;
import java.util.Objects;
import pl.cansoft.webcodersspring.dtos.image.ImageDto;
import pl.cansoft.webcodersspring.dtos.image.ImageSaveDto;
import pl.cansoft.webcodersspring.models.Image;

public class ImageMapper {

    public static List<ImageDto> mapImages(List<Image> images) {
        return images.stream()
            .map(ImageMapper::mapImage)
            .toList();
    }

    public static List<Image> mapImagesDto(List<ImageSaveDto> images) {
        return images.stream()
            .map(ImageMapper::mapImage)
            .toList();
    }

    public static Image mapImage(ImageSaveDto imageSaveDto) {
        return Image.builder()
            .uri(imageSaveDto.getUri())
            .build();
    }

    public static ImageDto mapImage(Image image) {
        return Objects.isNull(image) ? null : ImageDto.builder()
            .id(image.getId())
            .uri(image.getUri())
            .build();
    }
}
