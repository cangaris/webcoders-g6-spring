package pl.cansoft.webcodersspring.mappers;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import pl.cansoft.webcodersspring.dtos.category.CategorySimpleDto;
import pl.cansoft.webcodersspring.dtos.image.ImageDto;
import pl.cansoft.webcodersspring.dtos.product.ProductDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSaveDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSimpleDto;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.models.Image;
import pl.cansoft.webcodersspring.models.Product;
import pl.cansoft.webcodersspring.models.Tag;

public class ProductMapper {

    public static Product map(ProductSaveDto productDto) {
        List<Image> images = ImageMapper.mapImagesDto(productDto.getImages());
        return Product.builder()
            .name(productDto.getName())
            .content(productDto.getContent())
            .price(productDto.getPrice())
            .images(images)
            .build();
    }

    public static ProductDto map(Product product) {
        List<TagDto> tagDtos = mapTagsDto(product.getTags());
        List<Category> categories = product.getCategories();
        List<CategorySimpleDto> categoryDtos = CategoryMapper.mapListToSimpleDto(categories);
        List<ImageDto> imageDtos = ImageMapper.mapImages(product.getImages());
        return ProductDto.builder()
            .id(product.getId())
            .name(product.getName())
            .content(product.getContent())
            .price(product.getPrice())
            .categories(categoryDtos)
            .images(imageDtos)
            .tags(tagDtos)
            .build();
    }

    public static ProductSimpleDto mapToSimpleDto(Product product) {
        return ProductSimpleDto.builder()
            .id(product.getId())
            .name(product.getName())
            .content(product.getContent())
            .price(product.getPrice())
            .build();
    }

    public static Page<ProductDto> map(Page<Product> product) {
        return product.map(ProductMapper::map);
    }

    public static List<ProductSimpleDto> mapListToSimpleDto(List<Product> product) {
        return product.stream()
            .map(ProductMapper::mapToSimpleDto)
            .collect(Collectors.toList());
    }

    private static TagDto mapTagDto(Tag tag) {
        return TagDto.builder()
            .id(tag.getId())
            .name(tag.getName())
            .build();
    }

    private static List<TagDto> mapTagsDto(List<Tag> tags) {
        return tags.stream()
            .map(ProductMapper::mapTagDto)
            .toList();
    }
}
