package pl.cansoft.webcodersspring.services;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;
import pl.cansoft.webcodersspring.dtos.tag.TagSaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.mappers.TagMapper;
import pl.cansoft.webcodersspring.models.Tag;
import pl.cansoft.webcodersspring.repositories.TagRepository;

@Service
@RequiredArgsConstructor
public class TagService {

    private final TagRepository tagRepository;

    public Page<TagDto> getTags(Pageable pageable) {
        Page<Tag> tags = tagRepository.findAll(pageable);
        return TagMapper.mapListDto(tags);
    }

    public void insertTag(TagSaveDto dto) {
        if (tagRepository.existsByName(dto.getName())) {
            throw new ConflictException(List.of("Tag name: " + dto.getName()));
        }
        Tag tag = TagMapper.map(dto);
        tagRepository.save(tag);
    }

    public void updateTag(Integer id, TagSaveDto dto) {
        if (tagRepository.existsByNameAndIdNot(dto.getName(), id)) {
            throw new ConflictException(List.of("Tag name: " + dto.getName()));
        }
        Tag tag = TagMapper.map(dto);
        tag.setId(id);
        tagRepository.save(tag);
    }

    public void deleteTag(Integer id) {
        if (tagRepository.existsById(id)) {
            tagRepository.deleteById(id);
        } else {
            throw new NotFoundException("Tag not found");
        }
    }
}
