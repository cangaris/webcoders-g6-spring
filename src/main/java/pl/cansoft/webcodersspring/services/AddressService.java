package pl.cansoft.webcodersspring.services;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.address.AddressSaveDto;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.mappers.AddressMapper;
import pl.cansoft.webcodersspring.models.Address;
import pl.cansoft.webcodersspring.repositories.AddressRepository;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    public Page<AddressDto> getAddresses(Pageable pageable) {
        Page<Address> addresses = addressRepository.findAll(pageable);
        return AddressMapper.map(addresses);
    }

    public AddressDto getAddress(Integer id) {
        Optional<Address> optionalAddress = addressRepository.findById(id);
        if (optionalAddress.isPresent()) {
            Address address = optionalAddress.get();
            return AddressMapper.map(address);
        } else {
            throw new NotFoundException("Address not found");
        }
    }

    public void insertAddress(AddressSaveDto addressDto) {
        Address address = AddressMapper.map(addressDto);
        address.setId(null);
        addressRepository.save(address);
    }

    public void updateAddress(Integer id, AddressSaveDto addressDto) {
        boolean addressExists = addressRepository.existsById(id);
        if (addressExists) {
            Address address = AddressMapper.map(addressDto);
            address.setId(id);
            addressRepository.save(address);
        } else {
            throw new NotFoundException("Address not found");
        }
    }

    public void deleteAddress(Integer id) {
        boolean addressExists = addressRepository.existsById(id);
        if (addressExists) {
            addressRepository.deleteById(id);
        } else {
            throw new NotFoundException("Address not found");
        }
    }
}
