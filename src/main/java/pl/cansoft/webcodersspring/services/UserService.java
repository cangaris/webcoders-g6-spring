package pl.cansoft.webcodersspring.services;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.components.CheckAccessComponent;
import pl.cansoft.webcodersspring.dtos.user.UserDto;
import pl.cansoft.webcodersspring.dtos.user.UserSaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.mappers.UserMapper;
import pl.cansoft.webcodersspring.models.User;
import pl.cansoft.webcodersspring.repositories.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final CheckAccessComponent checkAccessComponent;
    private final PasswordEncoder passwordEncoder;

    public Page<UserDto> getUsers(Pageable pageable) {
        Page<User> users = this.userRepository.findAll(pageable);
        return UserMapper.map(users);
    }

    public UserDto getUser(Integer userId) {
        checkAccessComponent.checkLoggedUserHasAccessToResources(userId);
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get(); // 100% pewnosci ze jest user
            return UserMapper.map(user);
        } else {
            throw new NotFoundException("User not found");
        }
    }

    public void insertUser(UserSaveDto userDto) {
        boolean alreadyExists =
            this.userRepository.existsByEmailOrGovDataPeselOrGovDataNip(
                userDto.getEmail(), userDto.getPesel(), userDto.getNip());
        if (alreadyExists) {
            throw new ConflictException(List.of(userDto.getEmail(), userDto.getPesel(), userDto.getNip()));
        } else {
            User user = UserMapper.map(userDto);
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setId(null); // insert into bo null jako ID
            this.userRepository.save(user);
        }
    }

    public void updateUser(Integer userId, UserSaveDto userDto) {
        checkAccessComponent.checkLoggedUserHasAccessToResources(userId);
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            boolean alreadyExists = this.userRepository.existsByEmailOrGovDataPeselOrGovDataNipAndIdNot(
                userDto.getEmail(), userDto.getPesel(), userDto.getNip(), userId);
            if (alreadyExists) {
                throw new ConflictException(List.of(userDto.getEmail(), userDto.getPesel(), userDto.getNip()));
            } else {
                User currentUser = optionalUser.get();
                User user = UserMapper.map(userDto, currentUser);
                String hashedPassword = passwordEncoder.encode(user.getPassword());
                user.setPassword(hashedPassword);
                this.userRepository.save(user);
            }
        } else {
            throw new NotFoundException("User not found");
        }
    }

    public void deleteUser(Integer userId) {
        checkAccessComponent.checkLoggedUserHasAccessToResources(userId);
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            this.userRepository.deleteById(userId);
        } else {
            throw new NotFoundException("User not found");
        }
    }
}
