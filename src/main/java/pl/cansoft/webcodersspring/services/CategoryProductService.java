package pl.cansoft.webcodersspring.services;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.dtos.category_product.CategoryProductDto;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.models.Product;
import pl.cansoft.webcodersspring.repositories.CategoryRepository;
import pl.cansoft.webcodersspring.repositories.ProductRepository;

@Service
@RequiredArgsConstructor
public class CategoryProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public void connectCategoryProduct(CategoryProductDto dto) {
        Product product = productRepository.findById(dto.getProductId())
            .orElseThrow(() -> new NotFoundException("Product does not exist!"));

        Category category = categoryRepository.findById(dto.getCategoryId())
            .orElseThrow(() -> new NotFoundException("Category does not exist!"));

        List<Product> productList = category.getProducts(); // 0
        productList.add(product); // 1

        category.setProducts(productList); // 1
        categoryRepository.save(category);
    }
}
