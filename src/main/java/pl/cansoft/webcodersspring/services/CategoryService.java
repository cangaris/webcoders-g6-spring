package pl.cansoft.webcodersspring.services;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.dtos.category.CategoryDto;
import pl.cansoft.webcodersspring.dtos.category.CategorySaveDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.mappers.CategoryMapper;
import pl.cansoft.webcodersspring.models.Category;
import pl.cansoft.webcodersspring.repositories.CategoryRepository;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public Page<CategoryDto> getCategories(Pageable pageable) {
        Page<Category> categories = this.categoryRepository.findAll(pageable);
        return CategoryMapper.map(categories);
    }

    public CategoryDto getCategory(Integer id) {
        Optional<Category> optionalCategory = this.categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            Category category = optionalCategory.get();
            return CategoryMapper.map(category);
        } else {
            throw new NotFoundException("Category not found");
        }
    }

    public void addCategory(CategorySaveDto categoryDto) {
        boolean alreadyExists = this.categoryRepository.existsByName(categoryDto.getName());
        if (alreadyExists) {
            throw new ConflictException("name", categoryDto.getName());
        } else {
            Category category = CategoryMapper.map(categoryDto);
            category.setId(null);
            this.categoryRepository.save(category);
        }
    }

    public void updateCategory(Integer id, CategorySaveDto categoryDto) {
        Optional<Category> optionalCategory = this.categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            boolean alreadyExists = this.categoryRepository.existsByNameAndIdNot(categoryDto.getName(), id);
            if (alreadyExists) {
                throw new ConflictException("name", categoryDto.getName());
            } else {
                Category category = CategoryMapper.map(categoryDto);
                category.setId(id);
                this.categoryRepository.save(category);
            }
        } else {
            throw new NotFoundException("Category not found");
        }
    }

    public void deleteCategory(Integer id) {
        Optional<Category> optionalCategory = this.categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            this.categoryRepository.deleteById(id);
        } else {
            throw new NotFoundException("Category not found");
        }
    }
}
