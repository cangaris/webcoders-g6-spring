package pl.cansoft.webcodersspring.services;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.webcodersspring.dtos.product.ProductDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSaveDto;
import pl.cansoft.webcodersspring.dtos.product.ProductTagDto;
import pl.cansoft.webcodersspring.exceptions.ConflictException;
import pl.cansoft.webcodersspring.exceptions.NotFoundException;
import pl.cansoft.webcodersspring.mappers.ProductMapper;
import pl.cansoft.webcodersspring.models.Product;
import pl.cansoft.webcodersspring.models.Tag;
import pl.cansoft.webcodersspring.repositories.ProductRepository;
import pl.cansoft.webcodersspring.repositories.TagRepository;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final TagRepository tagRepository;

    public Page<ProductDto> getProducts(Pageable pageable) {
        Page<Product> products = this.productRepository.findAll(pageable);
        return ProductMapper.map(products);
    }

    public ProductDto getProduct(Integer productId) {
        Optional<Product> optionalProduct = this.productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            return ProductMapper.map(product);
        } else {
            throw new NotFoundException("Product not found");
        }
    }

    public void assignTagToProduct(ProductTagDto dto) {
        Tag tag = tagRepository.findById(dto.getTagId())
            .orElseThrow(() -> new NotFoundException("Tag not found!"));
        Product product = productRepository.findById(dto.getProductId())
            .orElseThrow(() -> new NotFoundException("Product not found!"));

        List<Tag> tags = product.getTags();
        tags.add(tag);
        product.setTags(tags);

        productRepository.save(product);
    }

    public void addProduct(ProductSaveDto productDto) {
        boolean alreadyExists = this.productRepository.existsByName(productDto.getName());
        if (alreadyExists) {
            throw new ConflictException("name", productDto.getName());
        } else {
            Product product = ProductMapper.map(productDto);
            product.setId(null);
            this.productRepository.save(product);
            // jezeli zapomnimy @Valid na parametrze metody to adnotacja zadziała na próbie zapisu do bazy
            // jest to zła praktyka bo walidacja odbywa się za późno
            // nie mamy kontroli nad kodem błędu, wykonują się niepotrzebnie linijki kodu nad zapisem
        }
    }

    public void updateProduct(Integer productId, ProductSaveDto productDto) {
        Optional<Product> optionalProduct = this.productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            boolean alreadyExists = this.productRepository.existsByNameAndIdNot(productDto.getName(), productId);
            if (alreadyExists) {
                throw new ConflictException("name", productDto.getName());
            } else {
                Product product = ProductMapper.map(productDto);
                product.setId(productId);
                this.productRepository.save(product);
            }
        } else {
            throw new NotFoundException("Product not found");
        }
    }

    public void deleteProduct(Integer id) {
        Optional<Product> optionalProduct = this.productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            this.productRepository.deleteById(id);
        } else {
            throw new NotFoundException("Product not found");
        }
    }
}
