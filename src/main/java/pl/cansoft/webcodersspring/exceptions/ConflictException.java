package pl.cansoft.webcodersspring.exceptions;

import java.util.List;

public class ConflictException extends RuntimeException {
    public ConflictException(String filedName, String value) {
        super("Conflict for field: " + filedName + " for value: " + value);
    }

    public ConflictException(List<String> value) {
        super("Conflict for data with values: " + value);
    }
}
