package pl.cansoft.webcodersspring.exceptions;

public class AccessDeniedException extends RuntimeException {
    public AccessDeniedException() {
        super("Access denied for that operation");
    }
}
