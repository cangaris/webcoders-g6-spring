package pl.cansoft.webcodersspring.configs;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.cansoft.webcodersspring.repositories.UserRepository;

@Configuration // tworzy bean'a (@Configuration, @RestController, @Controller, @Service, @Repository, @Component)
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class SecurityConfig {

    private final UserRepository userRepository; // wstrzyknięte poprzez DI @RequiredArgsConstructor

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
            .cors(corsConfigurer -> corsConfigurer.configurationSource(corsConfigurationSource()))
            .csrf(AbstractHttpConfigurer::disable) // wyłaczenie tokenów CSRF (przydatne przy projektach MVC)
            .formLogin(form -> form
                .loginPage("/login")
                .successHandler((request, response, auth) -> response.setStatus(HttpStatus.NO_CONTENT.value()))
                .failureHandler((request, response, exception) -> response.setStatus(HttpStatus.BAD_REQUEST.value()))
                .permitAll()
            )
            .logout(form -> form
                .logoutUrl("/logout")
                .logoutSuccessHandler((request, response, auth) -> response.setStatus(HttpStatus.NO_CONTENT.value()))
                .permitAll()
            )
            .exceptionHandling(handler -> handler
                .authenticationEntryPoint((req, response, ex) -> response.setStatus(HttpStatus.UNAUTHORIZED.value()))
            )
            .authorizeHttpRequests(auth ->
                auth
                    .requestMatchers(HttpMethod.POST, "/user").permitAll()
                    .requestMatchers(HttpMethod.GET, "/product").permitAll()
                    .requestMatchers(HttpMethod.GET, "/category").permitAll()
                    .requestMatchers(HttpMethod.GET, "/product/{id}").permitAll()
                    .requestMatchers(HttpMethod.GET, "/category/{id}").permitAll()
                    //.requestMatchers(HttpMethod.POST, "/product").hasRole("ADMIN") -> alternatywa @Secured("ROLE_ADMIN")
                    .anyRequest().authenticated())
            .build();
    }

    /*
     $2a$10$hyVZidlgjcvGHXLw1SbZ9uU2.suPdJw.MjJWqYMwKxLbfXtMFDoOG - Pass11PASS
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setPasswordEncoder(passwordEncoder());
        authProvider.setUserDetailsService(username ->
            userRepository.findByEmail(username)
                .map(user -> User.withUsername(user.getEmail())
                    .password(user.getPassword())
                    .roles(user.getRole())
                    .build())
                .orElseThrow(() -> new UsernameNotFoundException("Invalid credentials")));
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:4200")); // lista akceptowanych FRONTów
        configuration.setAllowedMethods(List.of("*")); // akeptuj wszytskie metody tj. GET, PUT, POST...
        configuration.setAllowedHeaders(List.of("*")); // akceptuj wszystkie headery zapytania http
        configuration.setAllowCredentials(true); // pozwalaj na przesyłanie danych logowania (JSESSIONID)
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
