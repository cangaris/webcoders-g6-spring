package pl.cansoft.webcodersspring.dtos.product;

import jakarta.validation.Valid;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.image.ImageSaveDto;

@Getter
@Setter
@Builder
public class ProductSaveDto {
    @NotBlank
    @Size(min = 5, max = 255)
    @Pattern(regexp = "[a-zA-Z0-9 ]+")
    private String name;

    @Pattern(regexp = "[a-zA-Z0-9 ]+")
    @Size(max = 2000)
    private String content;

    @DecimalMin("1.00")
    @DecimalMax("1000.00")
    private BigDecimal price;

    private List<@Valid ImageSaveDto> images;
}
