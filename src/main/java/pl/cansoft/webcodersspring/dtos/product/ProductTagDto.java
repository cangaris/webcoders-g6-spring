package pl.cansoft.webcodersspring.dtos.product;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductTagDto {
    @NotNull
    private Integer productId;
    @NotNull
    private Integer tagId;
}
