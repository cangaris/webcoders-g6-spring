package pl.cansoft.webcodersspring.dtos.product;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.category.CategorySimpleDto;
import pl.cansoft.webcodersspring.dtos.image.ImageDto;
import pl.cansoft.webcodersspring.dtos.tag.TagDto;

@Builder
@Getter
@Setter
public class ProductDto {
    private Integer id;
    private String name;
    private String content;
    private BigDecimal price;
    private List<ImageDto> images;
    private List<TagDto> tags;
    private List<CategorySimpleDto> categories;
}
