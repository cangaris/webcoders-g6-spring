package pl.cansoft.webcodersspring.dtos.product;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ProductSimpleDto {
    private Integer id;
    private String name;
    private String content;
    private BigDecimal price;
}
