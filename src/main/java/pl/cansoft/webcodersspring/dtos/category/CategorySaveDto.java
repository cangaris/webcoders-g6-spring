package pl.cansoft.webcodersspring.dtos.category;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.image.ImageSaveDto;

@Getter
@Setter
@Builder
public class CategorySaveDto {
    // @NotNull // sprawdzi tylko czy nie NULL
    // @NotEmpty // sprawdzi czy nie NULL oraz nie ""
    @NotBlank // sprawdzi czy nie NULL nie "" oraz nie "    " (spring ze spacjami)
    @Size(min = 5, max = 255)
    @Pattern(regexp = "[a-zA-Z ]+")
    private String name;

    @Pattern(regexp = "[a-zA-Z0-9 ]+")
    @Size(max = 2000)
    private String content;

    private List<@Valid ImageSaveDto> images;
}
