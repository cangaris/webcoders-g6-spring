package pl.cansoft.webcodersspring.dtos.category;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.image.ImageDto;
import pl.cansoft.webcodersspring.dtos.product.ProductSimpleDto;

@Builder
@Getter
@Setter
public class CategoryDto {
    private Integer id;
    private String name;
    private String content;
    private List<ImageDto> images;
    private List<ProductSimpleDto> products;
}
