package pl.cansoft.webcodersspring.dtos.category;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CategorySimpleDto {
    private Integer id;
    private String name;
    private String content;
}
