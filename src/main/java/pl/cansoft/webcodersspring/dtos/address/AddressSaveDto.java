package pl.cansoft.webcodersspring.dtos.address;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AddressSaveDto {
    @NotBlank
    @Size(min = 3, max = 100)
    private String city;

    @NotBlank // walidacja danych wejścowych do kontrolera
    @Size(min = 3, max = 100) // walidacja danych wejścowych do kontrolera
    private String street;
}
