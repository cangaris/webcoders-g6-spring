package pl.cansoft.webcodersspring.dtos.address;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder // dodaje builder
@Getter // wygeneruje gettery
@Setter // wygeneruje settery
public class AddressDto {
    private Integer id;
    private String city;
    private String street;
}
