package pl.cansoft.webcodersspring.dtos.user;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.address.AddressSaveDto;
import pl.cansoft.webcodersspring.dtos.phone.PhoneSaveDto;

@Getter
@Setter
@Builder
public class UserSaveDto {
    @NotBlank
    @Size(min = 3, max = 50)
    @Pattern(regexp = "[A-Z][a-z]+")
    private String firstName;

    @NotBlank
    @Size(min = 3, max = 50)
    @Pattern(regexp = "[A-Z][a-z]+")
    private String lastName;

    @Email
    @Size(min = 4, max = 100)
    private String email;

    @Pattern(regexp = "^\\d{11}$")
    @NotBlank
    @Size(min = 11, max = 11)
    private String pesel;

    @Pattern(regexp = "^\\d{10}$")
    @NotBlank
    @Size(min = 10, max = 10)
    private String nip;

    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
    @NotBlank
    @Size(min = 8, max = 255)
    private String password;

    @NotBlank
    @Pattern(regexp = "ADMIN|CLIENT")
    @Size(min = 3, max = 10)
    private String role;

    @Valid
    private PhoneSaveDto phone;

    private List<@Valid AddressSaveDto> addresses; // walidaowanie addresu wewnątrz usera
}
