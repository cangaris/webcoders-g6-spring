package pl.cansoft.webcodersspring.dtos.user;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.cansoft.webcodersspring.dtos.address.AddressDto;
import pl.cansoft.webcodersspring.dtos.phone.PhoneDto;

@Getter
@Setter
@Builder
public class UserDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String role;
    private String nip;
    private PhoneDto phone;
    private List<AddressDto> addresses;
}
