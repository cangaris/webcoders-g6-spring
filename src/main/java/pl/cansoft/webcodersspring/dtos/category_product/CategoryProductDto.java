package pl.cansoft.webcodersspring.dtos.category_product;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CategoryProductDto {
    @NotNull
    Integer productId;
    @NotNull
    Integer categoryId;
}
