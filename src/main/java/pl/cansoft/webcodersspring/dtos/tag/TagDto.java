package pl.cansoft.webcodersspring.dtos.tag;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TagDto {
    private Integer id;
    private String name;
}
