package pl.cansoft.webcodersspring.dtos.phone;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PhoneDto {
    private Integer id;
    private String prefix;
    private String number;
}
