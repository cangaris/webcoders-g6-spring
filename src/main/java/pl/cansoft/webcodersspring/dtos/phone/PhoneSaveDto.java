package pl.cansoft.webcodersspring.dtos.phone;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PhoneSaveDto {
    private String prefix;
    private String number;
}
