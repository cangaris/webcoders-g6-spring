#
# SQL Export
# Created by Querious (201069)
# Created: 14 January 2024 at 12:28:47 CET
# Encoding: Unicode (UTF-8)
#


SET @PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS = 0;


LOCK TABLES `gov_data` WRITE;
TRUNCATE `gov_data`;
ALTER TABLE `gov_data` DISABLE KEYS;
REPLACE INTO `gov_data` (`id`, `pesel`, `nip`) VALUES 
	(1,'00000000001','0000000001');
ALTER TABLE `gov_data` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `app_user` WRITE;
TRUNCATE `app_user`;
ALTER TABLE `app_user` DISABLE KEYS;
REPLACE INTO `app_user` (`id`, `first_name`, `last_name`, `email`, `password`, `role`, `gov_data_id`, `phone_id`) VALUES 
	(1,'Piotr','Nowak','pio@now1.pl','$2a$10$hyVZidlgjcvGHXLw1SbZ9uU2.suPdJw.MjJWqYMwKxLbfXtMFDoOG','CLIENT',1,1);
ALTER TABLE `app_user` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `address` WRITE;
TRUNCATE `address`;
ALTER TABLE `address` DISABLE KEYS;
REPLACE INTO `address` (`id`, `city`, `street`, `user_id`) VALUES 
	(1,'Kraków','Krakowska 3/3',1);
ALTER TABLE `address` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `category` WRITE;
TRUNCATE `category`;
ALTER TABLE `category` DISABLE KEYS;
REPLACE INTO `category` (`id`, `name`, `content`) VALUES 
	(1,'Servers test','the best Servers');
ALTER TABLE `category` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `product` WRITE;
TRUNCATE `product`;
ALTER TABLE `product` DISABLE KEYS;
REPLACE INTO `product` (`id`, `name`, `content`, `price`) VALUES 
	(1,'iPhone 13','opis',100);
ALTER TABLE `product` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `category_product_pivot` WRITE;
TRUNCATE `category_product_pivot`;
ALTER TABLE `category_product_pivot` DISABLE KEYS;
REPLACE INTO `category_product_pivot` (`category_id`, `product_id`) VALUES 
	(1,1);
ALTER TABLE `category_product_pivot` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `databasechangelog` WRITE;
TRUNCATE `databasechangelog`;
ALTER TABLE `databasechangelog` DISABLE KEYS;
REPLACE INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES 
	('raw','includeAll','db/changelog/migrations/001-create-phone.sql','2024-01-14 12:17:48',1,'EXECUTED','9:f280a17e76c1039ff99ac8b04217470d','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/002-create-gov-data.sql','2024-01-14 12:17:48',2,'EXECUTED','9:6e6085e513d3e8ca25953edfec4428b6','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/003-create-app-user.sql','2024-01-14 12:17:48',3,'EXECUTED','9:e53845c1022a22f5ac2af503fcbf3411','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/004-create-address.sql','2024-01-14 12:17:48',4,'EXECUTED','9:0d95d4e3afccfb5473f9945783db1ebb','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/005-create-tag.sql','2024-01-14 12:17:48',5,'EXECUTED','9:edd513efec8228e2d9faaf5417a7394b','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/006-create-product.sql','2024-01-14 12:17:48',6,'EXECUTED','9:ab0d374715be9ce410a679359574d7e1','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/007-create-product-tag-pivot.sql','2024-01-14 12:17:48',7,'EXECUTED','9:d01168101389ba7171eb1844db3fd91a','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/008-create-category.sql','2024-01-14 12:17:49',8,'EXECUTED','9:5e5e69311eabafd355feb3fdfbd6dafd','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/009-create-category-product-pivot.sql','2024-01-14 12:17:49',9,'EXECUTED','9:e7823d2290bbbe31a22ea5321daa3590','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/010-create-image.sql','2024-01-14 12:17:49',10,'EXECUTED','9:47e3f1576010e65674ade4cb70f6ca08','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/011-add-index.sql','2024-01-14 12:17:49',11,'EXECUTED','9:c393e28269766c9b70af2f36f3ccada9','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/012-add-index.sql','2024-01-14 12:17:49',12,'EXECUTED','9:82f5c3b246c7a84f6630b5190149718d','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/013-add-index.sql','2024-01-14 12:17:49',13,'EXECUTED','9:e2b634d1557ddc183d84bedfd5f773ad','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/014-add-index.sql','2024-01-14 12:17:49',14,'EXECUTED','9:d070294af354257e506fff7a21e4b988','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/015-add-index.sql','2024-01-14 12:17:49',15,'EXECUTED','9:5e54f09388dfa92dd86f18345fc91bb2','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/016-add-index.sql','2024-01-14 12:17:49',16,'EXECUTED','9:424e2cf994a14c4b45cb03beb64a489e','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/017-add-index.sql','2024-01-14 12:17:49',17,'EXECUTED','9:3deb6f16d2f10b3a5dcff11623d48d43','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/018-add-index.sql','2024-01-14 12:17:49',18,'EXECUTED','9:0bdf65eca16c1980c4a20ddd02d26e12','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/019-add-index.sql','2024-01-14 12:17:49',19,'EXECUTED','9:20e76047f3c3b40ef9eaf45211b85f73','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/020-add-index.sql','2024-01-14 12:17:49',20,'EXECUTED','9:b3e9e70b333775cf8d95aec50a7afac0','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/021-add-index.sql','2024-01-14 12:17:49',21,'EXECUTED','9:ce00b975f5acff43e3fab0bc12272420','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/022-add-index.sql','2024-01-14 12:17:49',22,'EXECUTED','9:bb70b33e160317d94636a4b720ba4b78','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/023-add-index.sql','2024-01-14 12:17:49',23,'EXECUTED','9:4efc483c327455478f0a9c55247364e5','sql','',NULL,'4.25.1',NULL,NULL,'5231068889'),
	('raw','includeAll','db/changelog/migrations/024-add-index.sql','2024-01-14 12:17:49',24,'EXECUTED','9:317cac4db6c3a4dc4c6a81828d20655a','sql','',NULL,'4.25.1',NULL,NULL,'5231068889');
ALTER TABLE `databasechangelog` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `databasechangeloglock` WRITE;
TRUNCATE `databasechangeloglock`;
ALTER TABLE `databasechangeloglock` DISABLE KEYS;
REPLACE INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES 
	(1,0,NULL,NULL);
ALTER TABLE `databasechangeloglock` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `image` WRITE;
TRUNCATE `image`;
ALTER TABLE `image` DISABLE KEYS;
REPLACE INTO `image` (`id`, `uri`, `product_id`, `category_id`) VALUES 
	(1,'..151.',NULL,1),
	(2,'..115.',NULL,1),
	(3,'..product.',1,NULL);
ALTER TABLE `image` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `phone` WRITE;
TRUNCATE `phone`;
ALTER TABLE `phone` DISABLE KEYS;
REPLACE INTO `phone` (`id`, `prefix`, `number`) VALUES 
	(1,'+48','666');
ALTER TABLE `phone` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `tag` WRITE;
TRUNCATE `tag`;
ALTER TABLE `tag` DISABLE KEYS;
REPLACE INTO `tag` (`id`, `name`) VALUES 
	(1,'tag');
ALTER TABLE `tag` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `product_tag_pivot` WRITE;
TRUNCATE `product_tag_pivot`;
ALTER TABLE `product_tag_pivot` DISABLE KEYS;
REPLACE INTO `product_tag_pivot` (`product_id`, `tag_id`) VALUES 
	(1,1);
ALTER TABLE `product_tag_pivot` ENABLE KEYS;
UNLOCK TABLES;




SET FOREIGN_KEY_CHECKS = @PREVIOUS_FOREIGN_KEY_CHECKS;


